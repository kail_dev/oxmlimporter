//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace OXmlImporter
{
    using System;
    using System.Collections.Generic;
    
    public partial class DokAtrybuty
    {
        public int DAt_DAtId { get; set; }
        public string DAt_Kod { get; set; }
        public int DAt_DeAId { get; set; }
        public string DAt_WartoscTxt { get; set; }
        public Nullable<int> DAt_TrNId { get; set; }
        public Nullable<int> DAt_CRKId { get; set; }
        public Nullable<int> DAt_SrZId { get; set; }
        public Nullable<int> DAt_VaNID { get; set; }
        public Nullable<int> DAt_DoNID { get; set; }
        public Nullable<int> DAt_OfdID { get; set; }
        public Nullable<int> DAt_DokumentTyp { get; set; }
        public Nullable<int> DAt_DokumentId { get; set; }
        public Nullable<byte> DAt_TypJPK { get; set; }
        public Nullable<decimal> DAt_WartoscDecimal { get; set; }
    
        public virtual CRMKontakty CRMKontakty { get; set; }
        public virtual DefAtrybuty DefAtrybuty { get; set; }
        public virtual OfertyDok OfertyDok { get; set; }
        public virtual SrsZlecenia SrsZlecenia { get; set; }
        public virtual VatNag VatNag { get; set; }
        public virtual TraNag TraNag { get; set; }
    }
}
