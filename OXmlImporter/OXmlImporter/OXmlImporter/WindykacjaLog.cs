//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace OXmlImporter
{
    using System;
    using System.Collections.Generic;
    
    public partial class WindykacjaLog
    {
        public int WiL_WiLId { get; set; }
        public System.DateTime WiL_Data { get; set; }
        public int WiL_Akcja { get; set; }
        public int WiL_AkcjaId { get; set; }
        public int WiL_Blad { get; set; }
        public string WiL_NumerDok { get; set; }
        public short WiL_PodmiotTyp { get; set; }
        public int WiL_PodmiotId { get; set; }
        public string WiL_Opis { get; set; }
        public int WiL_DokumentTyp { get; set; }
        public int WiL_DokumentId { get; set; }
        public int WiL_PlatnoscId { get; set; }
        public int WiL_Source { get; set; }
        public Nullable<System.Guid> WiL_DokumentGUID { get; set; }
    }
}
