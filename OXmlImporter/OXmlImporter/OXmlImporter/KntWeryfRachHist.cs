//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace OXmlImporter
{
    using System;
    using System.Collections.Generic;
    
    public partial class KntWeryfRachHist
    {
        public int KWRH_KWRHID { get; set; }
        public int KWRH_KntID { get; set; }
        public string KWRH_Nip { get; set; }
        public string KWRH_RachunekNr { get; set; }
        public System.DateTime KWRH_Data { get; set; }
        public string KWRH_IdOdpytania { get; set; }
        public int KWRH_Status { get; set; }
    
        public virtual Kontrahenci Kontrahenci { get; set; }
    }
}
