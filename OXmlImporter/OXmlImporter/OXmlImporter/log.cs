﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CDNBase;

using CDNTwrb1;

using CDNKH;
using NLog;

namespace OXmlImporter
{
  public  class log
    {

        private static Logger logger = LogManager.GetCurrentClassLogger();
        protected static Application Application = null;
        protected static ILogin Login = null;
        public bool LogowanieAutomatyczne()
        {
            string Operator = Properties.Settings.Default.user_optima;
            string Haslo = Properties.Settings.Default.pass_optima;
            string Firma = Properties.Settings.Default.baza_optima;

            object[] hPar = new object[]
            {
                0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0
            }; // do jakich modułów będzie logowanie
            /* Kolejno: KP, KH, KHP, ST, FA, MAG, PK, PKXL, CRM, ANL, DET, BIU, SRW, ODB, KB, KBP, HAP  
             */

            // katalog, gdzie jest zainstalowana Optima (bez ustawienia tej zmiennej nie zadziała, chyba że program odpalimy z katalogu O!)
            System.Environment.CurrentDirectory = @"C:\Program Files (x86)\Comarch ERP Optima";

            // tworzenie obiektu apliakcji
            Application = new Application();
            // blokowanie
            Application.LockApp(513, 5000, null, null, null, null);
            // logowanie do podanej Firmy, na danego operatora, do podanych modułów

            try
            {
                Console.WriteLine("Logowanie...");
                Login = Application.Login(Operator, Haslo, Firma, hPar[0], hPar[1], hPar[2], hPar[3], hPar[4], hPar[5],
                    hPar[6], hPar[7], hPar[8], hPar[9], hPar[10], hPar[11], hPar[12], hPar[13], hPar[14], hPar[15],
                    hPar[16]);
                Console.WriteLine("Prawidowe zalogowanie");
                return true;
               
            }
            catch
            {
                Console.WriteLine("Logowanie zakończono niepowodzeniem.");
             
                return false;
            }

            // tu jestemy zalogowani do O!            
        }

        public void Rabaty()
        {
            CDNBase.AdoSession Sesja = Login.CreateSession();

            var rabaty = (CDNTwrb1.Rabaty)Sesja.CreateObject("CDN.Rabaty", null);
            var rabat = (CDNTwrb1.IRabat)rabaty.AddNew();
            rabat.Cena = 1111;
            rabat.Typ = 6;
            rabat.TowarID = 26;
            rabat.PodmiotTyp = 1;
            rabat.PodmiotID = 3;




            Sesja.Save();
            //     Console.ReadKey();
            // 
            //  
            //
            //
        }
        public void Wylogowanie()
        {
            // niszczymy Login
            Login = null;
            // odblokowanie (wylogowanie) O!
            Application.UnlockApp();
            // niszczymy obiekt Aplikacji
            Application = null;
        }

        public bool Add(string kod, string opis, string iloscxml,double waga,string Url, string wymiar, string marka)
        {
            try
            {
                CDNBase.AdoSession Sesja = Login.CreateSession();

                var towary = (CDNTwrb1.Towary)Sesja.CreateObject("CDN.Towary", null);
                var towar = (CDNTwrb1.ITowar)towary["Twr_kod ='" + kod + "'"];
               


                towar.WagaKG = waga;
              
                towar.URL = Url;
            
                towar.Opis = opis;
                Sesja.Save();
                //TwA_DeAId
             

                try
                {

                    var atrybutwym = (ITwrAtrybut)towar.Atrybuty["TwA_DeAId ='" + Properties.Settings.Default.ATR_ID_WYMIAR + "'"];
                    atrybutwym.Wartosc = wymiar;
                    Sesja.Save();

                    var atrybut12 = (ITwrAtrybut)towar.Atrybuty["TwA_DeAId ='" + Properties.Settings.Default.ATR_ID + "'"];
                    
                        atrybut12.Wartosc = iloscxml;
                    Sesja.Save();

                    var aatr = (ITwrAtrybut)towar.Atrybuty["TwA_DeAId ='" + Properties.Settings.Default.ATR_XMLADD + "'"];
                    aatr.Wartosc = "TAK";
                    Sesja.Save();

                   
                }
            catch(Exception e)
                {
                    var atrybut2 = (ITwrAtrybut)towar.Atrybuty.AddNew();
                    atrybut2.DeAID = Properties.Settings.Default.ATR_ID_WYMIAR;
                    atrybut2.Wartosc = wymiar;
                    Sesja.Save();
                    var atrybut1 = (ITwrAtrybut)towar.Atrybuty.AddNew();
                    //35
                    atrybut1.DeAID = Properties.Settings.Default.ATR_ID;

                    atrybut1.Wartosc = iloscxml;


                    Sesja.Save();
                    var aatr = (ITwrAtrybut)towar.Atrybuty.AddNew();
                    aatr.DeAID = Properties.Settings.Default.ATR_XMLADD;
                    aatr.Wartosc = "TAK";
                    Sesja.Save();

                }

                var pr = (CDNBase.ICollection)(Sesja.CreateObject("CDN.Marki", null));
                try
                {
                    var pr1 = (OP_Twrb2Lib.Marka)pr["Mrk_Nazwa ='" + marka + "'"];
                    towar.MrkID = pr1.ID; Sesja.Save();
                }
                catch
                {
                    var pr1 = (OP_Twrb2Lib.Marka)pr.AddNew();
                    pr1.Nazwa = marka;
                    Sesja.Save();
                    towar.MrkID = pr1.ID; Sesja.Save();
                }
               


                //   return Sesja.();

                return true;

            }






            catch (Exception e)

            {
                logger.Error(e);
                return false;
                //GetSql sql = new GetSql();
                //var ids = sql.CheckTwrID(kod);
                //foreach (var id in ids)
                //{
                //  sql.UpdateQty(id.TWRXML_ID, Qty);
                // }
                //logger.Error("Towar o kodzie {0} juz istnieje. Aktualizuje ilosc na:{1}", kod, Qty);



            }
        }
        public bool UpdateXMLQTY(string kod, string ilosc)
        {
            try
            {

            
            ITwrAtrybut atrybut;
            
            CDNBase.AdoSession Sesja = Login.CreateSession();

            var towary = (CDNTwrb1.Towary)Sesja.CreateObject("CDN.Towary", null);
            var towar = (CDNTwrb1.ITowar)towary["Twr_kod ='" + kod + "'"];


                var atrybut12 = (ITwrAtrybut)towar.Atrybuty["TwA_DeAId ='" + Properties.Settings.Default.ATR_ID + "'"];

                atrybut12.Wartosc = ilosc;
               
                Sesja.Save();
                return true;
            }
            catch (Exception E)
            {
                return false;
            }

        }

        public bool DeleteAtr(string kod, int idatr)
        {
            try
            {


                ITwrAtrybut atrybut;

                CDNBase.AdoSession Sesja = Login.CreateSession();

                var towary = (CDNTwrb1.Towary)Sesja.CreateObject("CDN.Towary", null);
           
               


                               Sesja.Save();
                return true;
            }
            catch (Exception E)
            {
                return false;
            }

        }


    }
}
