//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace OXmlImporter
{
    using System;
    using System.Collections.Generic;
    
    public partial class VatZD
    {
        public int VaZ_VaZId { get; set; }
        public int VaZ_Bufor { get; set; }
        public Nullable<int> VaZ_BZdId { get; set; }
        public int VaZ_VaNId { get; set; }
        public Nullable<int> VaZ_VaNId_ZK { get; set; }
        public Nullable<int> VaZ_TrNId { get; set; }
        public int VaZ_Typ { get; set; }
        public int VaZ_DkNId { get; set; }
        public decimal VaZ_SumaKwotZdarzen { get; set; }
        public decimal VaZ_SumaKwotRozliczen { get; set; }
        public string VaZ_KntNazwa1 { get; set; }
        public string VaZ_KntNazwa2 { get; set; }
        public string VaZ_KntNazwa3 { get; set; }
        public string VaZ_KntNipE { get; set; }
        public string VaZ_Dokument { get; set; }
        public System.DateTime VaZ_DataWys { get; set; }
        public System.DateTime VaZ_Termin { get; set; }
        public Nullable<System.DateTime> VaZ_TerminPlatnosci { get; set; }
        public decimal VaZ_RazemNetto { get; set; }
        public decimal VaZ_RazemVAT { get; set; }
        public decimal VaZ_RazemBrutto { get; set; }
        public Nullable<int> VaZ_RodzajZakupu { get; set; }
        public Nullable<int> VaZ_Odliczenia { get; set; }
        public int VaZ_Flaga { get; set; }
        public decimal VaZ_Stawka { get; set; }
        public decimal VaZ_Netto { get; set; }
        public decimal VaZ_VAT { get; set; }
        public decimal VaZ_Brutto { get; set; }
        public decimal VaZ_KorektaNetto { get; set; }
        public decimal VaZ_KorektaVat { get; set; }
        public Nullable<decimal> VaZ_PoprzednioDoRoliczenia { get; set; }
        public Nullable<decimal> VaZ_PoprzednioRozliczono { get; set; }
        public Nullable<decimal> VaZ_DoRoliczeniaNaDzienObliczenia { get; set; }
        public Nullable<decimal> VaZ_RoliczonoNaDzienObliczenia { get; set; }
    
        public virtual DeklNag DeklNag { get; set; }
    }
}
