//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace OXmlImporter
{
    using System;
    using System.Collections.Generic;
    
    public partial class KsiRozrachunki
    {
        public int KRo_KRoId { get; set; }
        public Nullable<int> KRo_OObId { get; set; }
        public short KRo_Typ { get; set; }
        public string KRo_Dokument { get; set; }
        public string KRo_NrDziennika { get; set; }
        public string KRo_Dziennik { get; set; }
        public Nullable<int> KRo_NumerDziennika { get; set; }
        public string KRo_NrKsiegi { get; set; }
        public string KRo_IdentKsieg { get; set; }
        public Nullable<System.DateTime> KRo_DataDokumentu { get; set; }
        public Nullable<System.DateTime> KRo_DataOperacji { get; set; }
        public Nullable<System.DateTime> KRo_TerminPlatnosci { get; set; }
        public Nullable<System.DateTime> KRo_DataRozliczenia { get; set; }
        public Nullable<System.DateTime> KRo_DataRoz { get; set; }
        public Nullable<decimal> KRo_KwotaDok { get; set; }
        public Nullable<decimal> KRo_Kwota { get; set; }
        public string KRo_Konto { get; set; }
        public Nullable<decimal> KRo_SumRozliczen { get; set; }
        public string KRo_KontoIdx { get; set; }
        public string KRo_KontoPrzeciw { get; set; }
        public Nullable<int> KRo_DeEId { get; set; }
        public Nullable<byte> KRo_Bufor { get; set; }
        public Nullable<byte> KRo_Storno { get; set; }
        public Nullable<int> KRo_BOEId { get; set; }
        public string KRo_Opis { get; set; }
        public Nullable<int> KRo_RozliczenieId { get; set; }
        public Nullable<int> KRo_ParentId { get; set; }
        public Nullable<int> KRo_Strona { get; set; }
        public Nullable<decimal> KRo_KwotaDokWal { get; set; }
        public Nullable<decimal> KRo_KwotaWal { get; set; }
        public Nullable<decimal> KRo_SumRozliczenWal { get; set; }
        public string KRo_Waluta { get; set; }
        public Nullable<int> KRo_BRKID { get; set; }
    }
}
