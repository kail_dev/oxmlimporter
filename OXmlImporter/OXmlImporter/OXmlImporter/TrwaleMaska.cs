//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace OXmlImporter
{
    using System;
    using System.Collections.Generic;
    
    public partial class TrwaleMaska
    {
        public int SrM_SrMID { get; set; }
        public int SrM_SrTID { get; set; }
        public int SrM_RokMies { get; set; }
        public byte SrM_Typ { get; set; }
    
        public virtual Trwale Trwale { get; set; }
    }
}
