//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace OXmlImporter
{
    using System;
    using System.Collections.Generic;
    
    public partial class DeklStr
    {
        public int DKS_DksId { get; set; }
        public Nullable<int> DKS_DknId { get; set; }
        public int DKS_LP { get; set; }
        public string DKS_Tekst { get; set; }
    
        public virtual DeklNag DeklNag { get; set; }
    }
}
