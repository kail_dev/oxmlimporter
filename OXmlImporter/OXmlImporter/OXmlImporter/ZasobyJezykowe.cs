//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace OXmlImporter
{
    using System;
    using System.Collections.Generic;
    
    public partial class ZasobyJezykowe
    {
        public string ZJE_Id { get; set; }
        public int ZJE_Jezyk { get; set; }
        public string ZJE_Komunikat { get; set; }
    }
}
