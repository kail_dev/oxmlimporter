//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace OXmlImporter
{
    using System;
    using System.Collections.Generic;
    
    public partial class PracPitUdzialy
    {
        public int PPU_PPUID { get; set; }
        public int PPU_PPDID { get; set; }
        public decimal PPU_UdzialL { get; set; }
        public decimal PPU_UdzialM { get; set; }
        public System.DateTime PPU_DataOd { get; set; }
        public System.DateTime PPU_DataDo { get; set; }
        public decimal PPU_Przychody { get; set; }
        public decimal PPU_Koszty { get; set; }
        public decimal PPU_Stawka1Przychod { get; set; }
        public decimal PPU_Stawka2Przychod { get; set; }
        public decimal PPU_Stawka3Przychod { get; set; }
        public decimal PPU_Stawka4Przychod { get; set; }
        public decimal PPU_Stawka5Przychod { get; set; }
        public decimal PPU_Stawka6Przychod { get; set; }
        public decimal PPU_Stawka7Przychod { get; set; }
        public decimal PPU_Stawka8Przychod { get; set; }
        public decimal PPU_Stawka9Przychod { get; set; }
    
        public virtual PracPitDzialalnosci PracPitDzialalnosci { get; set; }
    }
}
