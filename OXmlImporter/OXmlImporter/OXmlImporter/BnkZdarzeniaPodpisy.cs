//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace OXmlImporter
{
    using System;
    using System.Collections.Generic;
    
    public partial class BnkZdarzeniaPodpisy
    {
        public int BZdP_BZdPID { get; set; }
        public int BZdP_BZdID { get; set; }
        public int BZdP_OpeID { get; set; }
        public System.DateTime BZdP_Data { get; set; }
        public string BZdP_CertyfikatNazwa { get; set; }
        public string BZdP_Podpis { get; set; }
    
        public virtual BnkZdarzenia BnkZdarzenia { get; set; }
    }
}
