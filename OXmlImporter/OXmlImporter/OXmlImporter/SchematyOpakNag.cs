//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace OXmlImporter
{
    using System;
    using System.Collections.Generic;
    
    public partial class SchematyOpakNag
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public SchematyOpakNag()
        {
            this.SchematyOpakElem = new HashSet<SchematyOpakElem>();
        }
    
        public int SON_SONId { get; set; }
        public byte SON_Nieaktywny { get; set; }
        public Nullable<System.DateTime> SON_TS_XL { get; set; }
        public string SON_Kod { get; set; }
        public string SON_Nazwa { get; set; }
        public Nullable<int> SON_OpeZalID { get; set; }
        public Nullable<int> SON_StaZalId { get; set; }
        public System.DateTime SON_TS_Zal { get; set; }
        public Nullable<int> SON_OpeModID { get; set; }
        public Nullable<int> SON_StaModID { get; set; }
        public System.DateTime SON_TS_Mod { get; set; }
        public string SON_OpeModKod { get; set; }
        public string SON_OpeModNazwisko { get; set; }
        public string SON_OpeZalKod { get; set; }
        public string SON_OpeZalNazwisko { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<SchematyOpakElem> SchematyOpakElem { get; set; }
    }
}
