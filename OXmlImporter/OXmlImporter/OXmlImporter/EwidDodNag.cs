//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace OXmlImporter
{
    using System;
    using System.Collections.Generic;
    
    public partial class EwidDodNag
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public EwidDodNag()
        {
            this.EwidDodElem = new HashSet<EwidDodElem>();
        }
    
        public int EDN_EDNID { get; set; }
        public Nullable<int> EDN_DDfId { get; set; }
        public string EDN_NumerString { get; set; }
        public int EDN_NumerNr { get; set; }
        public string EDN_NumerPelny { get; set; }
        public string EDN_NumerObcy { get; set; }
        public System.DateTime EDN_DataWys { get; set; }
        public System.DateTime EDN_DataDok { get; set; }
        public System.DateTime EDN_DataOpe { get; set; }
        public System.DateTime EDN_DataKur { get; set; }
        public int EDN_Typ { get; set; }
        public string EDN_Rejestr { get; set; }
        public Nullable<int> EDN_RokMies { get; set; }
        public int EDN_Lp { get; set; }
        public Nullable<short> EDN_PodmiotTyp { get; set; }
        public Nullable<int> EDN_PodID { get; set; }
        public string EDN_Nazwa1 { get; set; }
        public string EDN_Nazwa2 { get; set; }
        public string EDN_Nazwa3 { get; set; }
        public string EDN_Kraj { get; set; }
        public string EDN_Wojewodztwo { get; set; }
        public string EDN_Powiat { get; set; }
        public string EDN_Gmina { get; set; }
        public string EDN_Ulica { get; set; }
        public string EDN_NrDomu { get; set; }
        public string EDN_NrLokalu { get; set; }
        public string EDN_Miasto { get; set; }
        public string EDN_KodPocztowy { get; set; }
        public string EDN_Poczta { get; set; }
        public string EDN_Adres2 { get; set; }
        public string EDN_NipKraj { get; set; }
        public string EDN_NipE { get; set; }
        public Nullable<int> EDN_KatID { get; set; }
        public Nullable<short> EDN_PodmiotZalTyp { get; set; }
        public Nullable<int> EDN_PodZalId { get; set; }
        public Nullable<short> EDN_PlatnikTyp { get; set; }
        public Nullable<int> EDN_PlatnikID { get; set; }
        public Nullable<int> EDN_PlatnikRachunekLp { get; set; }
        public string EDN_PlatnikKraj { get; set; }
        public string EDN_PlatnikWojewodztwo { get; set; }
        public string EDN_PlatnikPowiat { get; set; }
        public string EDN_PlatnikGmina { get; set; }
        public string EDN_PlatnikUlica { get; set; }
        public string EDN_PlatnikNrDomu { get; set; }
        public string EDN_PlatnikNrLokalu { get; set; }
        public string EDN_PlatnikMiasto { get; set; }
        public string EDN_PlatnikKodPocztowy { get; set; }
        public string EDN_PlatnikPoczta { get; set; }
        public string EDN_PlatnikAdres2 { get; set; }
        public string EDN_PlatnikNazwa1 { get; set; }
        public string EDN_PlatnikNazwa2 { get; set; }
        public string EDN_PlatnikNazwa3 { get; set; }
        public string EDN_PlatnikRachunekNr { get; set; }
        public string EDN_Kategoria { get; set; }
        public decimal EDN_KwotaRazemSys { get; set; }
        public byte EDN_KontrolaSumy { get; set; }
        public decimal EDN_KwotaRazem { get; set; }
        public string EDN_Waluta { get; set; }
        public int EDN_KursNumer { get; set; }
        public decimal EDN_KursL { get; set; }
        public decimal EDN_KursM { get; set; }
        public byte EDN_Platnosci { get; set; }
        public Nullable<int> EDN_FPlId { get; set; }
        public System.DateTime EDN_Termin { get; set; }
        public string EDN_MagazynZrodlowy { get; set; }
        public string EDN_MagazynDocelowy { get; set; }
        public Nullable<int> EDN_DekId { get; set; }
        public Nullable<int> EDN_PreDekId { get; set; }
        public Nullable<int> EDN_KPRId { get; set; }
        public Nullable<int> EDN_RycId { get; set; }
        public string EDN_ImportAppId { get; set; }
        public string EDN_ImportRowId { get; set; }
        public Nullable<System.DateTime> EDN_TS_Export { get; set; }
        public Nullable<int> EDN_OpeZalID { get; set; }
        public Nullable<int> EDN_StaZalId { get; set; }
        public System.DateTime EDN_TS_Zal { get; set; }
        public Nullable<int> EDN_OpeModID { get; set; }
        public Nullable<int> EDN_StaModId { get; set; }
        public System.DateTime EDN_TS_Mod { get; set; }
        public Nullable<int> EDN_ZakID { get; set; }
        public Nullable<int> EDN_DnpID { get; set; }
        public string EDN_OpeModKod { get; set; }
        public string EDN_OpeModNazwisko { get; set; }
        public string EDN_OpeZalKod { get; set; }
        public string EDN_OpeZalNazwisko { get; set; }
    
        public virtual DokDefinicje DokDefinicje { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<EwidDodElem> EwidDodElem { get; set; }
        public virtual Kategorie Kategorie { get; set; }
        public virtual FormyPlatnosci FormyPlatnosci { get; set; }
        public virtual PodmiotyView PodmiotyView { get; set; }
    }
}
