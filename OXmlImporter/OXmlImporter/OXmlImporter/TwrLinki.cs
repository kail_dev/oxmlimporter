//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace OXmlImporter
{
    using System;
    using System.Collections.Generic;
    
    public partial class TwrLinki
    {
        public short TwL_GIDTyp { get; set; }
        public int TwL_GIDFirma { get; set; }
        public int TwL_GIDNumer { get; set; }
        public Nullable<short> TwL_GIDLp { get; set; }
        public short TwL_GrOTyp { get; set; }
        public int TwL_GrOFirma { get; set; }
        public int TwL_GrONumer { get; set; }
        public Nullable<short> TwL_GrOLp { get; set; }
        public Nullable<short> TwL_Lisc { get; set; }
        public Nullable<short> TwL_LiczbaPowtorzen { get; set; }
        public Nullable<int> TwL_TStampDataMod { get; set; }
    }
}
