//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace OXmlImporter
{
    using System;
    using System.Collections.Generic;
    
    public partial class OpisPlace
    {
        public int OPP_OppId { get; set; }
        public int OPP_WpeId { get; set; }
        public int OPP_PraId { get; set; }
        public int OPP_DzlId { get; set; }
        public int OPP_PrjId { get; set; }
        public decimal OPP_Procent { get; set; }
        public decimal OPP_Brutto { get; set; }
        public decimal OPP_ZUSF { get; set; }
        public decimal OPP_ZUSP { get; set; }
        public decimal OPP_Zdrow { get; set; }
        public decimal OPP_ZalFIS { get; set; }
        public decimal OPP_PPKP { get; set; }
        public decimal OPP_PPKF { get; set; }
    
        public virtual Dzialy Dzialy { get; set; }
        public virtual PracKod PracKod { get; set; }
        public virtual WypElementy WypElementy { get; set; }
    }
}
