//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace OXmlImporter
{
    using System;
    using System.Collections.Generic;
    
    public partial class AkordyHistWartosci
    {
        public int AHW_AhwId { get; set; }
        public int AHW_AkhId { get; set; }
        public decimal AHW_Ilosc { get; set; }
        public Nullable<System.DateTime> AHW_Czas { get; set; }
        public short AHW_Miesiac { get; set; }
        public int AHW_Rok { get; set; }
        public decimal AHW_Kwota { get; set; }
        public decimal AHW_Udzial { get; set; }
        public int AHW_Zmiana { get; set; }
    
        public virtual AkordyHist AkordyHist { get; set; }
    }
}
