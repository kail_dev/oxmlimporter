//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace OXmlImporter
{
    using System;
    using System.Collections.Generic;
    
    public partial class DetalKontrahenci
    {
        public int DKnt_DKntId { get; set; }
        public int DKnt_OptimaId { get; set; }
        public int DKnt_DSKntId { get; set; }
        public int DKnt_StanDetalID { get; set; }
        public string DKnt_Kod { get; set; }
        public string DKnt_EAN { get; set; }
        public string DKnt_Nazwa1 { get; set; }
        public string DKnt_Nazwa2 { get; set; }
        public string DKnt_Nazwa3 { get; set; }
        public string DKnt_Kraj { get; set; }
        public string DKnt_Wojewodztwo { get; set; }
        public string DKnt_Powiat { get; set; }
        public string DKnt_Gmina { get; set; }
        public string DKnt_Ulica { get; set; }
        public string DKnt_NrDomu { get; set; }
        public string DKnt_NrLokalu { get; set; }
        public string DKnt_Miasto { get; set; }
        public string DKnt_KodPocztowy { get; set; }
        public string DKnt_Poczta { get; set; }
        public string DKnt_Adres2 { get; set; }
        public string DKnt_NipKraj { get; set; }
        public string DKnt_NipE { get; set; }
        public string DKnt_Nip { get; set; }
        public string DKnt_Regon { get; set; }
        public string DKnt_Pesel { get; set; }
        public string DKnt_Telefon { get; set; }
        public string DKnt_Opis { get; set; }
        public string DKnt_OsobaOdbierajaca { get; set; }
        public int DKnt_StanSynchronizacji { get; set; }
        public System.DateTime DKnt_TS_Mod { get; set; }
        public int DKnt_OpeID { get; set; }
        public int DKnt_AdresId { get; set; }
        public int DKnt_AdresId2 { get; set; }
        public string DKnt_Telefon1 { get; set; }
        public string DKnt_eMail { get; set; }
        public string DKnt_URL { get; set; }
        public string DKnt_Fax { get; set; }
        public byte DKnt_Finalny { get; set; }
        public byte DKnt_Rodzaj_Dostawca { get; set; }
        public byte DKnt_Rodzaj_Odbiorca { get; set; }
        public byte DKnt_Rodzaj_Konkurencja { get; set; }
        public byte DKnt_Rodzaj_Partner { get; set; }
        public byte DKnt_Rodzaj_Potencjalny { get; set; }
    }
}
