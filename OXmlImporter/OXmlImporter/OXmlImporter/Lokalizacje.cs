//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace OXmlImporter
{
    using System;
    using System.Collections.Generic;
    
    public partial class Lokalizacje
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Lokalizacje()
        {
            this.Dzialy = new HashSet<Dzialy>();
        }
    
        public int Lok_LokId { get; set; }
        public string Lok_Kod { get; set; }
        public string Lok_Nazwa { get; set; }
        public string Lok_Symbol { get; set; }
        public string Lok_Konto { get; set; }
        public short Lok_Nieaktywny { get; set; }
        public string LOK_ImportAppId { get; set; }
        public string LOK_ImportRowId { get; set; }
        public Nullable<System.DateTime> LOK_TS_Export { get; set; }
        public Nullable<int> Lok_OpeZalId { get; set; }
        public Nullable<int> Lok_StaZalId { get; set; }
        public System.DateTime Lok_TS_Zal { get; set; }
        public Nullable<int> Lok_OpeModId { get; set; }
        public Nullable<int> Lok_StaModId { get; set; }
        public System.DateTime Lok_TS_Mod { get; set; }
        public string Lok_OpeModKod { get; set; }
        public string Lok_OpeModNazwisko { get; set; }
        public string Lok_OpeZalKod { get; set; }
        public string Lok_OpeZalNazwisko { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Dzialy> Dzialy { get; set; }
    }
}
