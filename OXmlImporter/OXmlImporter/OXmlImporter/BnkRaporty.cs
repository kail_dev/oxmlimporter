//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace OXmlImporter
{
    using System;
    using System.Collections.Generic;
    
    public partial class BnkRaporty
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public BnkRaporty()
        {
            this.BRpAgregaty = new HashSet<BRpAgregaty>();
            this.BnkZapisy = new HashSet<BnkZapisy>();
        }
    
        public int BRp_BRpID { get; set; }
        public Nullable<int> BRp_BRaID { get; set; }
        public Nullable<int> BRp_DDfID { get; set; }
        public int BRp_NumerNr { get; set; }
        public string BRp_NumerString { get; set; }
        public string BRp_NumerPelny { get; set; }
        public string BRp_NumerObcy { get; set; }
        public System.DateTime BRp_DataDok { get; set; }
        public Nullable<System.DateTime> BRp_DataZam { get; set; }
        public byte BRp_Zamkniety { get; set; }
        public Nullable<int> BRp_DekID { get; set; }
        public Nullable<int> BRp_PreDekID { get; set; }
        public decimal BRp_SaldoBO { get; set; }
        public decimal BRp_Przychody { get; set; }
        public decimal BRp_Rozchody { get; set; }
        public decimal BRp_SaldoBOSys { get; set; }
        public decimal BRp_PrzychodySys { get; set; }
        public decimal BRp_RozchodySys { get; set; }
        public decimal BRp_RoznicaKursowaSysMW { get; set; }
        public Nullable<byte> BRp_CDCWB { get; set; }
        public Nullable<System.DateTime> BRp_TS_Export { get; set; }
        public string BRp_ImportAppId { get; set; }
        public string BRp_ImportRowId { get; set; }
        public Nullable<int> BRp_OpeZalID { get; set; }
        public Nullable<int> BRp_StaZalId { get; set; }
        public System.DateTime BRp_TS_Zal { get; set; }
        public Nullable<int> BRp_OpeModID { get; set; }
        public Nullable<int> BRp_StaModId { get; set; }
        public System.DateTime BRp_TS_Mod { get; set; }
        public string BRp_OpeModKod { get; set; }
        public string BRp_OpeModNazwisko { get; set; }
        public string BRp_OpeZalKod { get; set; }
        public string BRp_OpeZalNazwisko { get; set; }
        public Nullable<short> BRp_GIDTyp { get; set; }
        public Nullable<int> BRp_GIDFirma { get; set; }
        public Nullable<int> BRp_GIDNumer { get; set; }
        public Nullable<short> BRp_GIDLp { get; set; }
    
        public virtual BnkRachunki BnkRachunki { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<BRpAgregaty> BRpAgregaty { get; set; }
        public virtual DokDefinicje DokDefinicje { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<BnkZapisy> BnkZapisy { get; set; }
    }
}
