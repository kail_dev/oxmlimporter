//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace OXmlImporter
{
    using System;
    using System.Collections.Generic;
    
    public partial class DlgElem
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public DlgElem()
        {
            this.DlgDokumenty = new HashSet<DlgDokumenty>();
            this.DlgTrasy = new HashSet<DlgTrasy>();
            this.DlgZaliczki = new HashSet<DlgZaliczki>();
        }
    
        public int DLE_DLEId { get; set; }
        public int DLE_DLNId { get; set; }
        public short DLE_RodzajEtapu { get; set; }
        public Nullable<int> DLE_KatId { get; set; }
        public string DLE_Kategoria { get; set; }
        public Nullable<System.DateTime> DLE_DataOd { get; set; }
        public Nullable<System.DateTime> DLE_DataDo { get; set; }
        public Nullable<System.DateTime> DLE_GodzinaOd { get; set; }
        public Nullable<System.DateTime> DLE_GodzinaDo { get; set; }
        public string DLE_Kraj { get; set; }
        public Nullable<decimal> DLE_StawkaDiety { get; set; }
        public Nullable<short> DLE_StawkaTyp { get; set; }
        public short DLE_WyplataZaliczkiSys { get; set; }
        public short DLE_RozliczenieSYS { get; set; }
        public short DLE_DataPoniesieniaKosztu { get; set; }
        public short DLE_KursRozliczenia { get; set; }
        public Nullable<System.DateTime> DLE_DataDok { get; set; }
        public Nullable<System.DateTime> DLE_DataKur { get; set; }
        public string DLE_Waluta { get; set; }
        public int DLE_KursNumer { get; set; }
        public decimal DLE_KursL { get; set; }
        public decimal DLE_KursM { get; set; }
        public decimal DLE_KwotaSys { get; set; }
        public decimal DLE_KwotaWal { get; set; }
        public Nullable<System.DateTime> DLE_KsiDataDok { get; set; }
        public Nullable<System.DateTime> DLE_KsiDataKur { get; set; }
        public string DLE_KsiWaluta { get; set; }
        public Nullable<int> DLE_KsiKursNumer { get; set; }
        public Nullable<decimal> DLE_KsiKursL { get; set; }
        public Nullable<decimal> DLE_KsiKursM { get; set; }
        public Nullable<decimal> DLE_KsiKwotaSys { get; set; }
        public Nullable<decimal> DLE_KsiKwotaWal { get; set; }
        public Nullable<System.DateTime> DLE_TerminPlatnosci { get; set; }
        public Nullable<int> DLE_FPlId { get; set; }
        public string DLE_ZapisKbBRaAkronim { get; set; }
        public Nullable<short> DLE_GenerujPlatnoscKB { get; set; }
        public Nullable<short> DLE_GenerujZapisKB { get; set; }
        public short DLE_SumujWyjazdPrzyjazd { get; set; }
        public Nullable<System.DateTime> DLE_IloscGodzinWyjazd { get; set; }
        public Nullable<System.DateTime> DLE_IloscGodzinPrzyjazd { get; set; }
        public Nullable<System.DateTime> DLE_IloscGodzinDelegacji { get; set; }
        public Nullable<decimal> DLE_DietyIlosc { get; set; }
        public Nullable<decimal> DLE_DietyKwota { get; set; }
        public Nullable<decimal> DLE_DietyKwotaSys { get; set; }
        public Nullable<decimal> DLE_KsiDietyKwota { get; set; }
        public Nullable<decimal> DLE_KsiDietyKwotaSys { get; set; }
        public Nullable<decimal> DLE_RazemDiety { get; set; }
        public Nullable<decimal> DLE_RazemDietySys { get; set; }
        public Nullable<decimal> DLE_KsiRazemDiety { get; set; }
        public Nullable<decimal> DLE_KsiRazemDietySys { get; set; }
        public Nullable<int> DLE_RyczaltDojazdyIlosc { get; set; }
        public Nullable<decimal> DLE_RyczaltDojazdyKwota { get; set; }
        public Nullable<decimal> DLE_RyczaltDojazdyKwotaSys { get; set; }
        public Nullable<decimal> DLE_KsiRyczaltDojazdyKwota { get; set; }
        public Nullable<decimal> DLE_KsiRyczaltDojazdyKwotaSys { get; set; }
        public Nullable<int> DLE_RyczaltNoclegiIlosc { get; set; }
        public Nullable<decimal> DLE_RyczaltNoclegiKwota { get; set; }
        public Nullable<decimal> DLE_RyczaltNoclegiKwotaSys { get; set; }
        public Nullable<decimal> DLE_KsiRyczaltNoclegiKwota { get; set; }
        public Nullable<decimal> DLE_KsiRyczaltNoclegiKwotaSys { get; set; }
        public Nullable<short> DLE_RyczaltNoclegiAuto { get; set; }
        public Nullable<short> DLE_RyczaltDojazdyAuto { get; set; }
        public Nullable<short> DLE_RyczaltDojazdyDworzecAuto { get; set; }
        public Nullable<decimal> DLE_RyczaltDojazdyDworzecIlosc { get; set; }
        public Nullable<decimal> DLE_RyczaltDojazdyDworzecKwota { get; set; }
        public Nullable<decimal> DLE_RyczaltDojazdyDworzecKwotaSys { get; set; }
        public Nullable<decimal> DLE_KsiRyczaltDojazdyDworzecKwota { get; set; }
        public Nullable<decimal> DLE_KsiRyczaltDojazdyDworzecKwotaSys { get; set; }
        public Nullable<short> DLE_RyczaltPobytSzpitalAuto { get; set; }
        public Nullable<int> DLE_RyczaltPobytSzpitalIlosc { get; set; }
        public Nullable<decimal> DLE_RyczaltPobytSzpitalKwota { get; set; }
        public Nullable<decimal> DLE_RyczaltPobytSzpitalKwotaSys { get; set; }
        public Nullable<decimal> DLE_KsiRyczaltPobytSzpitalKwota { get; set; }
        public Nullable<decimal> DLE_KsiRyczaltPobytSzpitalKwotaSys { get; set; }
        public string DLE_NumerRejestracyjny { get; set; }
        public Nullable<short> DLE_TypPojazdu { get; set; }
        public Nullable<decimal> DLE_PojazdStawkaZaKilometr { get; set; }
        public Nullable<int> DLE_PojazdIloscKilometrow { get; set; }
        public Nullable<decimal> DLE_PojazdKoszt { get; set; }
        public Nullable<decimal> DLE_PojazdKosztSys { get; set; }
        public Nullable<decimal> DLE_KsiPojazdKoszt { get; set; }
        public Nullable<decimal> DLE_KsiPojazdKosztSys { get; set; }
        public Nullable<int> DLE_IloscSniadan { get; set; }
        public Nullable<int> DLE_IloscObiadow { get; set; }
        public Nullable<int> DLE_IloscKolacji { get; set; }
        public Nullable<decimal> DLE_RazemRyczaltDojazdy { get; set; }
        public Nullable<decimal> DLE_RazemRyczaltDojazdySys { get; set; }
        public Nullable<decimal> DLE_RazemRyczaltNoclegi { get; set; }
        public Nullable<decimal> DLE_RazemRyczaltNoclegiSys { get; set; }
        public Nullable<decimal> DLE_RazemRyczaltDiety { get; set; }
        public Nullable<decimal> DLE_RazemRyczaltDietySys { get; set; }
        public Nullable<decimal> DLE_RazemInneWydatki { get; set; }
        public Nullable<decimal> DLE_RazemInneWydatkiSys { get; set; }
        public Nullable<decimal> DLE_RazemWydatki { get; set; }
        public Nullable<decimal> DLE_RazemWydatkiSys { get; set; }
        public Nullable<decimal> DLE_RazemZaliczka { get; set; }
        public Nullable<decimal> DLE_RazemZaliczkaSys { get; set; }
        public Nullable<decimal> DLE_RazemWydatkiBilety { get; set; }
        public Nullable<decimal> DLE_RazemWydatkiNoclegi { get; set; }
        public Nullable<decimal> DLE_RazemWydatkiInne { get; set; }
        public Nullable<int> DLE_RazemWydatkiBiletyIlosc { get; set; }
        public Nullable<int> DLE_RazemWydatkiNoclegiIlosc { get; set; }
        public Nullable<int> DLE_RazemWydatkiInneIlosc { get; set; }
        public Nullable<decimal> DLE_Rozliczono { get; set; }
        public Nullable<decimal> DLE_Pozostaje { get; set; }
        public Nullable<decimal> DLE_KsiRazemRyczaltDojazdy { get; set; }
        public Nullable<decimal> DLE_KsiRazemRyczaltDojazdySys { get; set; }
        public Nullable<decimal> DLE_KsiRazemRyczaltNoclegiSys { get; set; }
        public Nullable<decimal> DLE_KsiRazemRyczaltNoclegi { get; set; }
        public Nullable<decimal> DLE_KsiRazemRyczaltDietySys { get; set; }
        public Nullable<decimal> DLE_KsiRazemRyczaltDiety { get; set; }
        public Nullable<decimal> DLE_KsiRazemInneWydatkiSys { get; set; }
        public Nullable<decimal> DLE_KsiRazemInneWydatki { get; set; }
        public Nullable<decimal> DLE_KsiRazemWydatki { get; set; }
        public Nullable<decimal> DLE_KsiRazemWydatkiSys { get; set; }
        public Nullable<decimal> DLE_KsiRazemZaliczkaSys { get; set; }
        public Nullable<decimal> DLE_KsiRazemZaliczka { get; set; }
        public Nullable<decimal> DLE_RoznicaKursowa { get; set; }
        public Nullable<decimal> DLE_KwotaRozliczenia { get; set; }
        public Nullable<decimal> DLE_KwotaRozliczeniaSys { get; set; }
        public Nullable<decimal> DLE_KsiKwotaRozliczenia { get; set; }
        public Nullable<decimal> DLE_KsiKwotaRozliczeniaSys { get; set; }
        public string DLE_Opis { get; set; }
        public short DLE_WymagaPrzeliczenia { get; set; }
        public int DLE_Zrodlo { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<DlgDokumenty> DlgDokumenty { get; set; }
        public virtual DlgNag DlgNag { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<DlgTrasy> DlgTrasy { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<DlgZaliczki> DlgZaliczki { get; set; }
    }
}
