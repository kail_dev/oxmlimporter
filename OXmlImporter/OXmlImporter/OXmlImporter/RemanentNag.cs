//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace OXmlImporter
{
    using System;
    using System.Collections.Generic;
    
    public partial class RemanentNag
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public RemanentNag()
        {
            this.KorektaPodatkuDochodowego = new HashSet<KorektaPodatkuDochodowego>();
            this.RemanentElem = new HashSet<RemanentElem>();
        }
    
        public int ReN_ReNID { get; set; }
        public short ReN_Rok { get; set; }
        public short ReN_Numer { get; set; }
        public System.DateTime ReN_DataSpi { get; set; }
        public decimal ReN_Wartosc { get; set; }
        public decimal ReN_WartoscKorekta { get; set; }
        public Nullable<int> ReN_KatId { get; set; }
        public string ReN_Kategoria { get; set; }
        public string ReN_Opis { get; set; }
        public Nullable<int> ReN_KPRId { get; set; }
        public byte ReN_WartoscZbiorcza { get; set; }
        public Nullable<int> ReN_OpeZalID { get; set; }
        public Nullable<int> ReN_StaZalId { get; set; }
        public System.DateTime ReN_TS_Zal { get; set; }
        public Nullable<int> ReN_OpeModID { get; set; }
        public Nullable<int> ReN_StaModId { get; set; }
        public System.DateTime ReN_TS_Mod { get; set; }
        public byte ReN_Korekta { get; set; }
        public Nullable<int> ReN_ZakID { get; set; }
        public string ReN_OpeModKod { get; set; }
        public string ReN_OpeModNazwisko { get; set; }
        public string ReN_OpeZalKod { get; set; }
        public string ReN_OpeZalNazwisko { get; set; }
    
        public virtual Kategorie Kategorie { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<KorektaPodatkuDochodowego> KorektaPodatkuDochodowego { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<RemanentElem> RemanentElem { get; set; }
    }
}
