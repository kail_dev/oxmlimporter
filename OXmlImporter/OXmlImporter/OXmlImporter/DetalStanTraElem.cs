//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace OXmlImporter
{
    using System;
    using System.Collections.Generic;
    
    public partial class DetalStanTraElem
    {
        public int DSTrE_DSTrEID { get; set; }
        public int DSTrE_DSTrNId { get; set; }
        public int DSTrE_StornoId { get; set; }
        public int DSTrE_Lp { get; set; }
        public Nullable<int> DSTrE_ZwrId { get; set; }
        public int DSTrE_TwrId { get; set; }
        public string DSTrE_TwrNazwa { get; set; }
        public string DSTrE_TwrEAN { get; set; }
        public string DSTrE_TwrKod { get; set; }
        public decimal DSTrE_Stawka { get; set; }
        public short DSTrE_Flaga { get; set; }
        public decimal DSTrE_Cena { get; set; }
        public decimal DSTrE_Rabat { get; set; }
        public decimal DSTrE_CenaPoRabacie { get; set; }
        public byte DSTrE_Kaucja { get; set; }
        public decimal DSTrE_Ilosc { get; set; }
        public string DSTrE_Jm { get; set; }
        public byte DSTrE_JmCalkowite { get; set; }
        public decimal DSTrE_JMPrzelicznikL { get; set; }
        public decimal DSTrE_JMPrzelicznikM { get; set; }
        public string DSTrE_Waluta { get; set; }
        public decimal DSTrE_WartoscBrutto { get; set; }
    
        public virtual DetalStanTowary DetalStanTowary { get; set; }
        public virtual DetalStanTraNag DetalStanTraNag { get; set; }
    }
}
