//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace OXmlImporter
{
    using System;
    using System.Collections.Generic;
    
    public partial class Kategorie
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Kategorie()
        {
            this.AutoKoszty = new HashSet<AutoKoszty>();
            this.AutoRozlicz = new HashSet<AutoRozlicz>();
            this.BnkDokNag = new HashSet<BnkDokNag>();
            this.BnkNazwy = new HashSet<BnkNazwy>();
            this.BnkZapisy = new HashSet<BnkZapisy>();
            this.BnkZdarzenia = new HashSet<BnkZdarzenia>();
            this.BONag = new HashSet<BONag>();
            this.DekretyElem = new HashSet<DekretyElem>();
            this.DekretyNag = new HashSet<DekretyNag>();
            this.EwidDodElem = new HashSet<EwidDodElem>();
            this.EwidDodNag = new HashSet<EwidDodNag>();
            this.KwotyDodatkowe = new HashSet<KwotyDodatkowe>();
            this.KwotyDodatkowe1 = new HashSet<KwotyDodatkowe>();
            this.Ryczalt = new HashSet<Ryczalt>();
            this.Kontrahenci = new HashSet<Kontrahenci>();
            this.Kontrahenci1 = new HashSet<Kontrahenci>();
            this.KompensatyNag = new HashSet<KompensatyNag>();
            this.ZapisyKPR = new HashSet<ZapisyKPR>();
            this.NotyOdsNag = new HashSet<NotyOdsNag>();
            this.OkresoweElem = new HashSet<OkresoweElem>();
            this.OkresoweNag = new HashSet<OkresoweNag>();
            this.PracEtaty = new HashSet<PracEtaty>();
            this.RemanentNag = new HashSet<RemanentNag>();
            this.SrsCzesci = new HashSet<SrsCzesci>();
            this.SrsCyklElem = new HashSet<SrsCyklElem>();
            this.TrwaleHist = new HashSet<TrwaleHist>();
            this.TrwaleAINag = new HashSet<TrwaleAINag>();
            this.Trwale = new HashSet<Trwale>();
            this.SrsCyklNag = new HashSet<SrsCyklNag>();
            this.SrsCzynnosci = new HashSet<SrsCzynnosci>();
            this.SrsZlecenia = new HashSet<SrsZlecenia>();
            this.TraElem = new HashSet<TraElem>();
            this.TraNag = new HashSet<TraNag>();
            this.Towary = new HashSet<Towary>();
            this.Towary1 = new HashSet<Towary>();
            this.Urzedy = new HashSet<Urzedy>();
            this.VatNag = new HashSet<VatNag>();
            this.VatTab = new HashSet<VatTab>();
            this.VatTab1 = new HashSet<VatTab>();
            this.WypElementy = new HashSet<WypElementy>();
            this.Wyplaty = new HashSet<Wyplaty>();
            this.WyplatyElem = new HashSet<WyplatyElem>();
            this.WyplatyNag = new HashSet<WyplatyNag>();
            this.WyposazenieAINag = new HashSet<WyposazenieAINag>();
            this.Wyposazenie = new HashSet<Wyposazenie>();
        }
    
        public int Kat_KatID { get; set; }
        public short Kat_Typ { get; set; }
        public short Kat_Poziom { get; set; }
        public Nullable<int> Kat_ParentID { get; set; }
        public string Kat_KodOgolny { get; set; }
        public string Kat_KodSzczegol { get; set; }
        public string Kat_Opis { get; set; }
        public byte Kat_KolumnaKPIR { get; set; }
        public byte Kat_KolumnaRycz { get; set; }
        public Nullable<short> Kat_WzID { get; set; }
        public decimal Kat_Stawka { get; set; }
        public short Kat_Flaga { get; set; }
        public decimal Kat_Zrodlowa { get; set; }
        public short Kat_Odliczenia { get; set; }
        public short Kat_RodzajZakupu { get; set; }
        public byte Kat_Fiskalny { get; set; }
        public byte Kat_Detal { get; set; }
        public decimal Kat_Kwota { get; set; }
        public decimal Kat_Udzial { get; set; }
        public decimal Kat_Budzet { get; set; }
        public byte Kat_Nieaktywny { get; set; }
        public string Kat_KontoSegmentWN { get; set; }
        public string Kat_KontoSegmentMA { get; set; }
        public Nullable<System.DateTime> Kat_TS_Uzyc { get; set; }
        public string Kat_ElixirO1 { get; set; }
        public string Kat_ElixirO2 { get; set; }
        public string Kat_ElixirO3 { get; set; }
        public string Kat_ElixirO4 { get; set; }
        public byte Kat_PodzielOdliczenia { get; set; }
        public decimal Kat_PodzielOdliczeniaProcent { get; set; }
        public byte Kat_KsiegujWKoszty { get; set; }
        public decimal Kat_KsiegujWKosztyProcent { get; set; }
        public Nullable<System.DateTime> Kat_TS_Export { get; set; }
        public string Kat_ImportAppId { get; set; }
        public string Kat_ImportRowId { get; set; }
        public Nullable<int> Kat_OpeZalID { get; set; }
        public Nullable<int> Kat_StaZalId { get; set; }
        public System.DateTime Kat_TS_Zal { get; set; }
        public Nullable<int> Kat_OpeModID { get; set; }
        public Nullable<int> Kat_StaModId { get; set; }
        public System.DateTime Kat_TS_Mod { get; set; }
        public string Kat_OpeModKod { get; set; }
        public string Kat_OpeModNazwisko { get; set; }
        public string Kat_OpeZalKod { get; set; }
        public string Kat_OpeZalNazwisko { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<AutoKoszty> AutoKoszty { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<AutoRozlicz> AutoRozlicz { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<BnkDokNag> BnkDokNag { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<BnkNazwy> BnkNazwy { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<BnkZapisy> BnkZapisy { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<BnkZdarzenia> BnkZdarzenia { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<BONag> BONag { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<DekretyElem> DekretyElem { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<DekretyNag> DekretyNag { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<EwidDodElem> EwidDodElem { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<EwidDodNag> EwidDodNag { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<KwotyDodatkowe> KwotyDodatkowe { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<KwotyDodatkowe> KwotyDodatkowe1 { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Ryczalt> Ryczalt { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Kontrahenci> Kontrahenci { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Kontrahenci> Kontrahenci1 { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<KompensatyNag> KompensatyNag { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ZapisyKPR> ZapisyKPR { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<NotyOdsNag> NotyOdsNag { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OkresoweElem> OkresoweElem { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OkresoweNag> OkresoweNag { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<PracEtaty> PracEtaty { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<RemanentNag> RemanentNag { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<SrsCzesci> SrsCzesci { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<SrsCyklElem> SrsCyklElem { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<TrwaleHist> TrwaleHist { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<TrwaleAINag> TrwaleAINag { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Trwale> Trwale { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<SrsCyklNag> SrsCyklNag { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<SrsCzynnosci> SrsCzynnosci { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<SrsZlecenia> SrsZlecenia { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<TraElem> TraElem { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<TraNag> TraNag { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Towary> Towary { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Towary> Towary1 { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Urzedy> Urzedy { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<VatNag> VatNag { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<VatTab> VatTab { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<VatTab> VatTab1 { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<WypElementy> WypElementy { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Wyplaty> Wyplaty { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<WyplatyElem> WyplatyElem { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<WyplatyNag> WyplatyNag { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<WyposazenieAINag> WyposazenieAINag { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Wyposazenie> Wyposazenie { get; set; }
    }
}
