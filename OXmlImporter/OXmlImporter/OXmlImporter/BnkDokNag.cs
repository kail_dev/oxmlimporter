//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace OXmlImporter
{
    using System;
    using System.Collections.Generic;
    
    public partial class BnkDokNag
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public BnkDokNag()
        {
            this.BnkDokElem = new HashSet<BnkDokElem>();
        }
    
        public int BDN_BDNId { get; set; }
        public short BDN_Typ { get; set; }
        public Nullable<int> BDN_DDfId { get; set; }
        public int BDN_NumerNr { get; set; }
        public string BDN_NumerString { get; set; }
        public string BDN_NumerPelny { get; set; }
        public System.DateTime BDN_DataDok { get; set; }
        public Nullable<short> BDN_PodmiotTyp { get; set; }
        public Nullable<int> BDN_PodmiotID { get; set; }
        public Nullable<int> BDN_KatID { get; set; }
        public string BDN_Kategoria { get; set; }
        public string BDN_Waluta { get; set; }
        public int BDN_KursNumer { get; set; }
        public decimal BDN_KursL { get; set; }
        public decimal BDN_KursM { get; set; }
        public decimal BDN_RazemKwota1 { get; set; }
        public decimal BDN_RazemKwotaSys1 { get; set; }
        public decimal BDN_RazemKwota2 { get; set; }
        public decimal BDN_RazemKwotaSys2 { get; set; }
        public byte BDN_OdsetkiTyp { get; set; }
        public decimal BDN_OdsetkiStopa { get; set; }
        public decimal BDN_OdsetkiPodatkoweProg { get; set; }
        public decimal BDN_KosztyDodatkowe { get; set; }
        public decimal BDN_KosztyDodatkoweSys { get; set; }
        public byte BDN_GenerujPlatnosciKst { get; set; }
        public Nullable<int> BDN_FPlIDKst { get; set; }
        public Nullable<System.DateTime> BDN_TerminKst { get; set; }
        public byte BDN_GenerujPlatnosciOds { get; set; }
        public Nullable<int> BDN_FPlIDOds { get; set; }
        public Nullable<System.DateTime> BDN_TerminOds { get; set; }
        public string BDN_Uwagi { get; set; }
        public Nullable<int> BDN_DekID { get; set; }
        public Nullable<int> BDN_PreDekID { get; set; }
        public int BDN_Windykacja { get; set; }
        public byte BDN_UwzglNalez { get; set; }
        public byte BDN_UwzglZobow { get; set; }
        public byte BDN_UwzglPrzych { get; set; }
        public byte BDN_UwzglRozch { get; set; }
        public byte BDN_UwzglBufor { get; set; }
        public Nullable<int> BDN_OpeZalID { get; set; }
        public Nullable<int> BDN_StaZalId { get; set; }
        public System.DateTime BDN_TS_Zal { get; set; }
        public Nullable<int> BDN_OpeModID { get; set; }
        public Nullable<int> BDN_StaModId { get; set; }
        public System.DateTime BDN_TS_Mod { get; set; }
        public string BDN_OpeModKod { get; set; }
        public string BDN_OpeModNazwisko { get; set; }
        public string BDN_OpeZalKod { get; set; }
        public string BDN_OpeZalNazwisko { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<BnkDokElem> BnkDokElem { get; set; }
        public virtual DokDefinicje DokDefinicje { get; set; }
        public virtual Kategorie Kategorie { get; set; }
        public virtual PodmiotyView PodmiotyView { get; set; }
    }
}
