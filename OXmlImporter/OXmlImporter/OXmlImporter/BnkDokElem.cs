//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace OXmlImporter
{
    using System;
    using System.Collections.Generic;
    
    public partial class BnkDokElem
    {
        public int BDE_BDEId { get; set; }
        public int BDE_BDNId { get; set; }
        public string BDE_DokTyp { get; set; }
        public Nullable<int> BDE_DokId { get; set; }
        public string BDE_Numer { get; set; }
        public string BDE_Opis { get; set; }
        public System.DateTime BDE_DataDok { get; set; }
        public System.DateTime BDE_Termin { get; set; }
        public decimal BDE_Kwota1 { get; set; }
        public decimal BDE_Kwota2 { get; set; }
        public decimal BDE_Kwota { get; set; }
    
        public virtual BnkDokNag BnkDokNag { get; set; }
    }
}
