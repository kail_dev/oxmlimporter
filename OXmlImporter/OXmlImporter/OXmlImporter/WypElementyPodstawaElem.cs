//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace OXmlImporter
{
    using System;
    using System.Collections.Generic;
    
    public partial class WypElementyPodstawaElem
    {
        public int WEE_WeeId { get; set; }
        public int WEE_WepId { get; set; }
        public string WEE_Nazwa { get; set; }
        public string WEE_NumerPelny { get; set; }
        public byte WEE_Wlicz { get; set; }
        public byte WEE_WliczMetoda { get; set; }
        public byte WEE_RodzajElem { get; set; }
        public decimal WEE_Kwota1 { get; set; }
        public decimal WEE_Kwota2 { get; set; }
        public string WEE_Wspolczynnik { get; set; }
        public decimal WEE_Kwota3 { get; set; }
        public string WEE_GodzDni { get; set; }
        public Nullable<System.DateTime> WEE_OkresOd { get; set; }
        public Nullable<System.DateTime> WEE_OkresDo { get; set; }
    
        public virtual WypElementyPodstawa WypElementyPodstawa { get; set; }
    }
}
