//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace OXmlImporter
{
    using System;
    using System.Collections.Generic;
    
    public partial class BnkZdarzeniaAutoryzacje
    {
        public int BZdA_BZdAID { get; set; }
        public int BZdA_BZdID { get; set; }
        public int BZdA_OpeID { get; set; }
        public string BZdA_LoginUzytkownika { get; set; }
        public System.DateTime BZdA_Data { get; set; }
        public byte BZdA_TypAutoryzacji { get; set; }
    
        public virtual BnkZdarzenia BnkZdarzenia { get; set; }
    }
}
