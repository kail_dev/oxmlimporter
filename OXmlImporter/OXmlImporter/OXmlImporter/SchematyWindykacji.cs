//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace OXmlImporter
{
    using System;
    using System.Collections.Generic;
    
    public partial class SchematyWindykacji
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public SchematyWindykacji()
        {
            this.SchematWindykacjiEtapy = new HashSet<SchematWindykacjiEtapy>();
        }
    
        public int SwN_SwNId { get; set; }
        public string SwN_Kod { get; set; }
        public string SwN_Nazwa { get; set; }
        public byte SwN_Domyslny { get; set; }
        public byte SwN_Nieaktywny { get; set; }
        public Nullable<int> SwN_OpeZalId { get; set; }
        public Nullable<int> SwN_StaZalId { get; set; }
        public System.DateTime SwN_TS_Zal { get; set; }
        public Nullable<int> SwN_OpeModId { get; set; }
        public Nullable<int> SwN_StaModId { get; set; }
        public System.DateTime SwN_TS_Mod { get; set; }
        public string SwN_OpeModKod { get; set; }
        public string SwN_OpeModNazwisko { get; set; }
        public string SwN_OpeZalKod { get; set; }
        public string SwN_OpeZalNazwisko { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<SchematWindykacjiEtapy> SchematWindykacjiEtapy { get; set; }
    }
}
