//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace OXmlImporter
{
    using System;
    using System.Collections.Generic;
    
    public partial class OkresyObrach
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public OkresyObrach()
        {
            this.BONag = new HashSet<BONag>();
            this.Dzienniki = new HashSet<Dzienniki>();
            this.GrupyKontNag = new HashSet<GrupyKontNag>();
            this.Konta = new HashSet<Konta>();
            this.KragKosztNag = new HashSet<KragKosztNag>();
        }
    
        public int OOb_OObID { get; set; }
        public string OOb_Symbol { get; set; }
        public System.DateTime OOb_DataOtw { get; set; }
        public short OOb_Dlugosc { get; set; }
        public System.DateTime OOb_DataKoncowa { get; set; }
        public Nullable<System.DateTime> OOb_DataZam { get; set; }
        public int OOb_CiaglaNumeracjaDC { get; set; }
        public int OOb_ZaliczkiUproszczone { get; set; }
        public short OOb_Stan { get; set; }
        public string OOb_Opis { get; set; }
        public Nullable<int> OOb_DziIdRK { get; set; }
        public Nullable<int> OOb_DziIdKomp { get; set; }
        public short OOb_DataKsiRk { get; set; }
        public short OOb_DataKsiKomp { get; set; }
        public Nullable<int> OOb_OpeZalID { get; set; }
        public Nullable<int> OOb_StaZalId { get; set; }
        public System.DateTime OOb_TS_Zal { get; set; }
        public Nullable<int> OOb_OpeModID { get; set; }
        public Nullable<int> OOb_StaModId { get; set; }
        public System.DateTime OOb_TS_Mod { get; set; }
        public string OOb_OpeModKod { get; set; }
        public string OOb_OpeModNazwisko { get; set; }
        public string OOb_OpeZalKod { get; set; }
        public string OOb_OpeZalNazwisko { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<BONag> BONag { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Dzienniki> Dzienniki { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<GrupyKontNag> GrupyKontNag { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Konta> Konta { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<KragKosztNag> KragKosztNag { get; set; }
    }
}
