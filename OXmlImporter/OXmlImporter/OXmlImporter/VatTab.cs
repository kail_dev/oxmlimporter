//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace OXmlImporter
{
    using System;
    using System.Collections.Generic;
    
    public partial class VatTab
    {
        public int VaT_VaTID { get; set; }
        public int VaT_VaNID { get; set; }
        public Nullable<int> VaT_VaTOrgId { get; set; }
        public decimal VaT_Stawka { get; set; }
        public short VaT_Flaga { get; set; }
        public decimal VaT_Zrodlowa { get; set; }
        public short VaT_RodzajZakupu { get; set; }
        public short VaT_Odliczenia { get; set; }
        public decimal VaT_Netto { get; set; }
        public decimal VaT_NettoDoVAT { get; set; }
        public decimal VaT_VAT { get; set; }
        public decimal VaT_VATDoVAT { get; set; }
        public decimal VaT_NettoWal { get; set; }
        public decimal VaT_VATWal { get; set; }
        public Nullable<int> VaT_KatID { get; set; }
        public string VaT_KatOpis { get; set; }
        public Nullable<int> VaT_Kat2ID { get; set; }
        public string VaT_Kat2Opis { get; set; }
        public byte VaT_KolumnaKPR { get; set; }
        public byte VaT_KolumnaRYC { get; set; }
        public int VaT_Lp { get; set; }
        public decimal VaT_KwotaNKUP { get; set; }
        public decimal VaT_VATNKUP { get; set; }
        public string VaT_StawkaSymbol { get; set; }
    
        public virtual Kategorie Kategorie { get; set; }
        public virtual Kategorie Kategorie1 { get; set; }
        public virtual VatNag VatNag { get; set; }
    }
}
