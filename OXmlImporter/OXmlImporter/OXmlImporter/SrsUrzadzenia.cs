//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace OXmlImporter
{
    using System;
    using System.Collections.Generic;
    
    public partial class SrsUrzadzenia
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public SrsUrzadzenia()
        {
            this.TwrAtrybuty = new HashSet<TwrAtrybuty>();
        }
    
        public int SrU_SrUId { get; set; }
        public string SrU_Kod { get; set; }
        public string SrU_Nazwa { get; set; }
        public string SrU_EAN { get; set; }
        public Nullable<short> SrU_PodmiotTyp { get; set; }
        public Nullable<int> SrU_PodmiotId { get; set; }
        public Nullable<short> SrU_OdbiorcaTyp { get; set; }
        public Nullable<int> SrU_OdbID { get; set; }
        public Nullable<int> SrU_SrRId { get; set; }
        public Nullable<int> SrU_TwRId { get; set; }
        public string SrU_Opis { get; set; }
        public byte SrU_FCzynnosci { get; set; }
        public byte SrU_FCzesci { get; set; }
        public Nullable<int> SrU_OpeZalId { get; set; }
        public Nullable<int> SrU_StaZalId { get; set; }
        public System.DateTime SrU_TS_Zal { get; set; }
        public Nullable<int> SrU_OpeModId { get; set; }
        public Nullable<int> SrU_StaModId { get; set; }
        public System.DateTime SrU_TS_Mod { get; set; }
        public string SrU_OpeModKod { get; set; }
        public string SrU_OpeModNazwisko { get; set; }
        public string SrU_OpeZalKod { get; set; }
        public string SrU_OpeZalNazwisko { get; set; }
    
        public virtual PodmiotyView PodmiotyView { get; set; }
        public virtual PodmiotyView PodmiotyView1 { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<TwrAtrybuty> TwrAtrybuty { get; set; }
    }
}
