﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OXmlImporter
{
    public class sql
    {


        public List<Towary> Opisy()
        {
            Entities2 en = new Entities2();
            var result = en.Towary.Where(x => x.Twr_Opis.Length > 0).ToList();
            return result;
        }

        public int idtwr(string twrkod)
        {
            Entities2 en = new Entities2();
            var res = en.Towary.Where(x => x.Twr_Kod == twrkod).Select(k => k.Twr_TwrId).FirstOrDefault();
            return res;
        }

        public decimal cenatwr(int twrgid)
        {
            Entities2 en = new Entities2();
            var res = en.TwrCeny.Where(x => x.TwC_TwrID == twrgid && x.TwC_TwCNumer == Properties.Settings.Default.CENA_ID).Select(y => y.TwC_Wartosc).FirstOrDefault();
            return res;

        }



    }
}
