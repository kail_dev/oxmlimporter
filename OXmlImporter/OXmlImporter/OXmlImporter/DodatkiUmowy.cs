//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace OXmlImporter
{
    using System;
    using System.Collections.Generic;
    
    public partial class DodatkiUmowy
    {
        public int DOU_DouId { get; set; }
        public int DOU_DodId { get; set; }
        public int DOU_UmwId { get; set; }
    
        public virtual Dodatki Dodatki { get; set; }
        public virtual Umowy Umowy { get; set; }
    }
}
