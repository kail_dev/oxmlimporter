//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace OXmlImporter
{
    using System;
    using System.Collections.Generic;
    
    public partial class TwrIlosci
    {
        public int TwI_TwIId { get; set; }
        public int TwI_TwrId { get; set; }
        public Nullable<int> TwI_MagId { get; set; }
        public Nullable<System.DateTime> TwI_Data { get; set; }
        public decimal TwI_Ilosc { get; set; }
        public decimal TwI_Wartosc { get; set; }
        public decimal TwI_Braki { get; set; }
        public decimal TwI_Rezerwacje { get; set; }
        public decimal TwI_Zamowienia { get; set; }
    
        public virtual Magazyny Magazyny { get; set; }
        public virtual Towary Towary { get; set; }
    }
}
