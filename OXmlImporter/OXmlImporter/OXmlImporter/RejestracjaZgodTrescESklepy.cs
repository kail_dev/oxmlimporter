//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace OXmlImporter
{
    using System;
    using System.Collections.Generic;
    
    public partial class RejestracjaZgodTrescESklepy
    {
        public int RZTES_RZTESID { get; set; }
        public int RZTES_RZTID { get; set; }
        public int RZTES_eSklepID { get; set; }
        public byte RZTES_eSklepWyslana { get; set; }
        public byte RZTES_PrzyszlaZeSklep { get; set; }
    
        public virtual eSklepStanowiska eSklepStanowiska { get; set; }
        public virtual RejestracjaZgodTresc RejestracjaZgodTresc { get; set; }
    }
}
