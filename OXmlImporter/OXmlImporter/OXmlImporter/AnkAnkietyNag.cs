//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace OXmlImporter
{
    using System;
    using System.Collections.Generic;
    
    public partial class AnkAnkietyNag
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public AnkAnkietyNag()
        {
            this.AnkAnkietyAnkieterzy = new HashSet<AnkAnkietyAnkieterzy>();
            this.AnkAnkietyPyt = new HashSet<AnkAnkietyPyt>();
        }
    
        public int AnkAnN_AnkAnNId { get; set; }
        public int AnkAnN_DDfId { get; set; }
        public string AnkAnN_NumerString { get; set; }
        public int AnkAnN_NumerNr { get; set; }
        public string AnkAnN_NumerPelny { get; set; }
        public System.DateTime AnkAnN_DataDok { get; set; }
        public int AnkAnN_Stan { get; set; }
        public string AnkAnN_PrzyczynaOdrzucenia { get; set; }
        public Nullable<int> AnkAnN_AnkWaNId { get; set; }
        public string AnkAnN_Nazwa { get; set; }
        public string AnkAnN_Opis { get; set; }
        public short AnkAnN_PodmiotTyp { get; set; }
        public int AnkAnN_PodID { get; set; }
        public Nullable<int> AnkAnN_OsobaId { get; set; }
        public string AnkAnN_OsobaTelefon { get; set; }
        public string AnkAnN_OsobaNazwisko { get; set; }
        public System.DateTime AnkAnN_TerminOd { get; set; }
        public System.DateTime AnkAnN_TerminDo { get; set; }
        public decimal AnkAnN_Ocena { get; set; }
        public Nullable<System.DateTime> AnkAnN_DataWypelnienia { get; set; }
        public Nullable<System.DateTime> AnkAnN_DataZatwierdzenia { get; set; }
        public Nullable<int> AnkAnN_Eksport { get; set; }
        public Nullable<int> AnkAnN_OpeZalID { get; set; }
        public Nullable<int> AnkAnN_StaZalId { get; set; }
        public System.DateTime AnkAnN_TS_Zal { get; set; }
        public Nullable<int> AnkAnN_OpeModID { get; set; }
        public Nullable<int> AnkAnN_StaModId { get; set; }
        public System.DateTime AnkAnN_TS_Mod { get; set; }
        public string AnkAnN_OpeModKod { get; set; }
        public string AnkAnN_OpeModNazwisko { get; set; }
        public string AnkAnN_OpeZalKod { get; set; }
        public string AnkAnN_OpeZalNazwisko { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<AnkAnkietyAnkieterzy> AnkAnkietyAnkieterzy { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<AnkAnkietyPyt> AnkAnkietyPyt { get; set; }
    }
}
