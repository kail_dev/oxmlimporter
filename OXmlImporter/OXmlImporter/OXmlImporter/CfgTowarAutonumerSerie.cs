//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace OXmlImporter
{
    using System;
    using System.Collections.Generic;
    
    public partial class CfgTowarAutonumerSerie
    {
        public int CTAS_CTASId { get; set; }
        public string CTAS_Nazwa { get; set; }
        public string CTAS_Opis { get; set; }
        public byte CTAS_Domyslna { get; set; }
    }
}
