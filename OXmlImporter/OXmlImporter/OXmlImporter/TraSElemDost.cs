//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace OXmlImporter
{
    using System;
    using System.Collections.Generic;
    
    public partial class TraSElemDost
    {
        public int TsD_TsDID { get; set; }
        public int TsD_TrSID { get; set; }
        public int TsD_TrEId { get; set; }
        public int TsD_PdEId { get; set; }
        public Nullable<int> TsD_TrSIdDost { get; set; }
        public Nullable<int> TsD_ZwrId { get; set; }
        public string TsD_NumerPelny { get; set; }
        public string TsD_NumerObcy { get; set; }
        public System.DateTime TsD_DataOpe { get; set; }
        public string TsD_Dostawca { get; set; }
        public decimal TsD_Ilosc { get; set; }
        public decimal TsD_IloscDostepna { get; set; }
        public decimal TsD_Wartosc { get; set; }
        public int TsD_Cecha1_DeAId { get; set; }
        public short TsD_Cecha1_Format { get; set; }
        public string TsD_Cecha1_Kod { get; set; }
        public string TsD_Cecha1_Wartosc { get; set; }
        public int TsD_Cecha2_DeAId { get; set; }
        public short TsD_Cecha2_Format { get; set; }
        public string TsD_Cecha2_Kod { get; set; }
        public string TsD_Cecha2_Wartosc { get; set; }
        public int TsD_Cecha3_DeAId { get; set; }
        public short TsD_Cecha3_Format { get; set; }
        public string TsD_Cecha3_Kod { get; set; }
        public string TsD_Cecha3_Wartosc { get; set; }
        public int TsD_Cecha4_DeAId { get; set; }
        public short TsD_Cecha4_Format { get; set; }
        public string TsD_Cecha4_Kod { get; set; }
        public string TsD_Cecha4_Wartosc { get; set; }
        public int TsD_Cecha5_DeAId { get; set; }
        public short TsD_Cecha5_Format { get; set; }
        public string TsD_Cecha5_Kod { get; set; }
        public string TsD_Cecha5_Wartosc { get; set; }
        public int TsD_Cecha6_DeAId { get; set; }
        public short TsD_Cecha6_Format { get; set; }
        public string TsD_Cecha6_Kod { get; set; }
        public string TsD_Cecha6_Wartosc { get; set; }
        public int TsD_Cecha7_DeAId { get; set; }
        public short TsD_Cecha7_Format { get; set; }
        public string TsD_Cecha7_Kod { get; set; }
        public string TsD_Cecha7_Wartosc { get; set; }
        public int TsD_Cecha8_DeAId { get; set; }
        public short TsD_Cecha8_Format { get; set; }
        public string TsD_Cecha8_Kod { get; set; }
        public string TsD_Cecha8_Wartosc { get; set; }
        public int TsD_Cecha9_DeAId { get; set; }
        public short TsD_Cecha9_Format { get; set; }
        public string TsD_Cecha9_Kod { get; set; }
        public string TsD_Cecha9_Wartosc { get; set; }
        public int TsD_Cecha10_DeAId { get; set; }
        public short TsD_Cecha10_Format { get; set; }
        public string TsD_Cecha10_Kod { get; set; }
        public string TsD_Cecha10_Wartosc { get; set; }
    
        public virtual TraElem TraElem { get; set; }
    }
}
