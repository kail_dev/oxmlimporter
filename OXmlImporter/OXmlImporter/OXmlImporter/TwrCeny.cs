//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace OXmlImporter
{
    using System;
    using System.Collections.Generic;
    
    public partial class TwrCeny
    {
        public int TwC_TwCID { get; set; }
        public int TwC_TwrID { get; set; }
        public int TwC_TwCNumer { get; set; }
        public byte TwC_Typ { get; set; }
        public decimal TwC_Wartosc { get; set; }
        public string TwC_Waluta { get; set; }
        public byte TwC_Aktualizacja { get; set; }
        public decimal TwC_Zaokraglenie { get; set; }
        public decimal TwC_Marza { get; set; }
        public decimal TwC_MarzaWStu { get; set; }
        public decimal TwC_Offset { get; set; }
        public Nullable<short> TwC_TwrTyp { get; set; }
        public Nullable<int> TwC_TwrFirma { get; set; }
        public Nullable<int> TwC_TwrNumer { get; set; }
        public Nullable<short> TwC_TwrLp { get; set; }
        public int TwC_DokID { get; set; }
        public decimal TwC_WartoscZakOld { get; set; }
        public int TwC_Punkty { get; set; }
    
        public virtual Towary Towary { get; set; }
    }
}
