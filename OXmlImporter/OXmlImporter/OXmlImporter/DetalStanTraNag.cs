//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace OXmlImporter
{
    using System;
    using System.Collections.Generic;
    
    public partial class DetalStanTraNag
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public DetalStanTraNag()
        {
            this.DetalStanPlatnosciDokumentu = new HashSet<DetalStanPlatnosciDokumentu>();
            this.DetalStanTraElem = new HashSet<DetalStanTraElem>();
        }
    
        public int DSTrN_DSTrNID { get; set; }
        public int DSTrN_StanDetalID { get; set; }
        public int DSTrN_FaId { get; set; }
        public Nullable<int> DSTrN_ZwrId { get; set; }
        public int DSTrN_TypDokumentu { get; set; }
        public int DSTrN_NumerNr { get; set; }
        public string DSTrN_NumerString { get; set; }
        public string DSTrN_NumerPelny { get; set; }
        public short DSTrN_Bufor { get; set; }
        public string DSTrN_Opis { get; set; }
        public int DSTrN_Anulowany { get; set; }
        public System.DateTime DSTrN_DataDok { get; set; }
        public byte DSTrN_Fiskalna { get; set; }
        public Nullable<int> DSTrN_FiskalnaErr { get; set; }
        public Nullable<int> DSTrN_PodID { get; set; }
        public Nullable<int> DSTrN_OdbID { get; set; }
        public decimal DSTrN_RazemBrutto { get; set; }
        public decimal DSTrN_Rabat { get; set; }
        public string DSTrN_Waluta { get; set; }
        public string DSTrN_OsobaOdbierajaca { get; set; }
        public Nullable<int> DSTrN_StanSynchronizacji { get; set; }
        public int DSTrN_OpeID { get; set; }
        public int DSTrN_ZmianaId { get; set; }
        public Nullable<byte> DSTrN_Korekta { get; set; }
        public int DSTrN_PARodzajKor { get; set; }
        public System.DateTime DSTrN_TS_Zal { get; set; }
    
        public virtual DetalStanKontrahenci DetalStanKontrahenci { get; set; }
        public virtual DetalStanOperatorzy DetalStanOperatorzy { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<DetalStanPlatnosciDokumentu> DetalStanPlatnosciDokumentu { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<DetalStanTraElem> DetalStanTraElem { get; set; }
    }
}
