//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace OXmlImporter
{
    using System;
    using System.Collections.Generic;
    
    public partial class TwrEan
    {
        public int TwE_TwEId { get; set; }
        public int TwE_TwrID { get; set; }
        public string TwE_EAN { get; set; }
        public string TwE_Opis { get; set; }
        public byte TwE_Domyslny { get; set; }
        public string TwE_JM { get; set; }
    
        public virtual Towary Towary { get; set; }
    }
}
