//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace OXmlImporter
{
    using System;
    using System.Collections.Generic;
    
    public partial class ZajeciaWynagr
    {
        public int ZAW_ZawId { get; set; }
        public int ZAW_PraId { get; set; }
        public int ZAW_KntId { get; set; }
        public int ZAW_TwpId { get; set; }
        public string ZAW_Sygnatura { get; set; }
        public Nullable<System.DateTime> ZAW_DataDecyzji { get; set; }
        public byte ZAW_Nieaktywne { get; set; }
        public Nullable<int> ZAW_BnaId { get; set; }
        public string ZAW_RachunekNr { get; set; }
        public string ZAW_RachunekNr0 { get; set; }
        public short ZAW_IBAN { get; set; }
        public string ZAW_Nazwa { get; set; }
        public string ZAW_Opis { get; set; }
        public Nullable<byte> ZAW_Rodzaj { get; set; }
        public Nullable<decimal> ZAW_Wartosc { get; set; }
        public Nullable<decimal> ZAW_Splacono { get; set; }
        public Nullable<System.DateTime> ZAW_DataOd { get; set; }
        public Nullable<System.DateTime> ZAW_DataDo { get; set; }
        public Nullable<byte> ZAW_Umowa { get; set; }
        public Nullable<byte> ZAW_UmowaOgranicz { get; set; }
        public Nullable<decimal> ZAW_UmowaOgraniczKwota { get; set; }
        public Nullable<decimal> ZAW_UmowaOgraniczProcent { get; set; }
        public Nullable<int> ZAW_Priorytet { get; set; }
        public byte ZAW_Splacone { get; set; }
        public Nullable<byte> ZAW_SplaconeCale { get; set; }
        public Nullable<System.DateTime> ZAW_IloscGodzin { get; set; }
        public int ZAW_RodzajCzasuPracy { get; set; }
        public byte ZAW_ProcWynZgodnyGrupa { get; set; }
        public Nullable<int> ZAW_OpeZalId { get; set; }
        public Nullable<int> ZAW_StaZalId { get; set; }
        public System.DateTime ZAW_TS_Zal { get; set; }
        public Nullable<int> ZAW_OpeModId { get; set; }
        public Nullable<int> ZAW_StaModId { get; set; }
        public System.DateTime ZAW_TS_Mod { get; set; }
        public string ZAW_OpeModKod { get; set; }
        public string ZAW_OpeModNazwisko { get; set; }
        public string ZAW_OpeZalKod { get; set; }
        public string ZAW_OpeZalNazwisko { get; set; }
    
        public virtual PracKod PracKod { get; set; }
    }
}
