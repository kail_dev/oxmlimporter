//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace OXmlImporter
{
    using System;
    using System.Collections.Generic;
    
    public partial class BRpAgregaty
    {
        public int BAg_BAgId { get; set; }
        public Nullable<int> BAg_BRpId { get; set; }
        public Nullable<short> BAg_Typ { get; set; }
        public Nullable<decimal> BAg_SaldoBOSys { get; set; }
        public Nullable<decimal> BAg_PrzychodySys { get; set; }
        public Nullable<decimal> BAg_RozchodySys { get; set; }
    
        public virtual BnkRaporty BnkRaporty { get; set; }
    }
}
