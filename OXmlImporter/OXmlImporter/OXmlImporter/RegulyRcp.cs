//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace OXmlImporter
{
    using System;
    using System.Collections.Generic;
    
    public partial class RegulyRcp
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public RegulyRcp()
        {
            this.ZestawyRegulElem = new HashSet<ZestawyRegulElem>();
        }
    
        public int RRG_RrgId { get; set; }
        public string RRG_Opis { get; set; }
        public int RRG_IloscMinZrodlo { get; set; }
        public byte RRG_Akcja { get; set; }
        public int RRG_MetodaModyf { get; set; }
        public int RRG_IloscMinCel { get; set; }
        public string RRG_ZjeId { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ZestawyRegulElem> ZestawyRegulElem { get; set; }
    }
}
