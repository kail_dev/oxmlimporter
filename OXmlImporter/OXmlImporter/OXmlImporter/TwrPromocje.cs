//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace OXmlImporter
{
    using System;
    using System.Collections.Generic;
    
    public partial class TwrPromocje
    {
        public int TPR_Id { get; set; }
        public Nullable<int> TPR_PrmId { get; set; }
        public Nullable<short> TPR_TwrTyp { get; set; }
        public Nullable<int> TPR_TwrFirma { get; set; }
        public Nullable<int> TPR_TwrNumer { get; set; }
        public Nullable<short> TPR_TwrLp { get; set; }
        public Nullable<short> TPR_TwgTyp { get; set; }
        public Nullable<int> TPR_TwgFirma { get; set; }
        public Nullable<int> TPR_TwgNumer { get; set; }
        public Nullable<short> TPR_TwgLp { get; set; }
        public Nullable<int> TPR_Typ { get; set; }
        public Nullable<decimal> TPR_Wartosc { get; set; }
        public Nullable<decimal> TPR_Prog { get; set; }
        public string TPR_Waluta { get; set; }
        public string TPR_FlagaNB { get; set; }
        public string TPR_Notatki { get; set; }
        public Nullable<int> TPR_ZstID { get; set; }
        public Nullable<int> TPR_LP { get; set; }
        public Nullable<int> TPR_RodzajCeny { get; set; }
    
        public virtual PrmKarty PrmKarty { get; set; }
    }
}
