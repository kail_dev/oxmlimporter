//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace OXmlImporter
{
    using System;
    using System.Collections.Generic;
    
    public partial class TypWyplataStaz
    {
        public int TWS_TwsId { get; set; }
        public int TWS_TwpId { get; set; }
        public int TWS_Staz { get; set; }
        public decimal TWS_Procent { get; set; }
    
        public virtual TypWyplata TypWyplata { get; set; }
    }
}
