﻿using CsvHelper;
using LinqToExcel;
using Microsoft.Win32;
using NLog;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Serialization;
namespace OXmlImporter
{

    public class Simple
    {
        /* Set the element name and namespace of the XML element.
        By applying an XmlElementAttribute to an array,  you instruct
        the XmlSerializer to serialize the array as a series of XML
        elements, instead of a nested set of elements. */




        [XmlElement(
        ElementName = "Product")
       ]
        public Product[] Products;
    }


        public class Product
    {
       

        public string title { get; set; }
        public string gtin { get; set; }
        public string price { get; set; }
        public int Qty { get; set; }
    }

    public partial class feedLink
    {

        private string relField;

        private string typeField;

        private string hrefField;

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string rel
        {
            get
            {
                return this.relField;
            }
            set
            {
                this.relField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string type
        {
            get
            {
                return this.typeField;
            }
            set
            {
                this.typeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string href
        {
            get
            {
                return this.hrefField;
            }
            set
            {
                this.hrefField = value;
            }
        }
    }
    public class LinkV2
    {
        [XmlAttribute("rel")]
        public string Url { get; set; }

        [XmlAttribute("type")]
        public string typ { get; set; }
        [XmlAttribute("href")]
        public string href { get; set; }
    }


    public class Group
    {
        /* Set the element name and namespace of the XML element.
        By applying an XmlElementAttribute to an array,  you instruct
        the XmlSerializer to serialize the array as a series of XML
        elements, instead of a nested set of elements. */
        private string titleField;
        private feedLink linkField;

        private System.DateTime updatedField;
        public string title
        {
            get
            {
                return this.titleField;
            }
            set
            {
                this.titleField = value;
            }
        }




        [XmlElement(DataType = "string",
     ElementName = "Title")]
        public string Title;


        private string m_id;
        private string m_name;
        private int m_age;
        private string[] m_prevcomp;

        public LinkV2 link;

        

        [XmlElement(DataType = "string",
     ElementName = "updated")]
        public string url;


        [XmlElement(
        ElementName = "entry")
       ]
        public Employee[] Employees;





    }

    public class Employee
    {
        public string id { get; set; }

        public string title { get; set; }

        public string description { get; set; }

        public string google_product_category { get; set; }

        public string product_type { get; set; }

        public string link { get; set; }


        public string condition { get; set; }

        public string availability { get; set; }
        public string inventory { get; set; }

        public string price { get; set; }

        public string brand { get; set; }

        public string gtin { get; set; }

        public string mpn { get; set; }

        public string shipping_weight { get; set; }
        public string image_link { get; set; }
        [XmlElement]

        public string[] additional_image_link { get; set; }
    }

    // [XmlRootAttribute("owner")]
    [XmlRoot]

    public class feed1
    {
        public string title { get; set; }

        public string link { get; set; }

        public string updated { get; set; }

        public entry1[] entry { get; set; }

       

    }

    [XmlType("entry")]

    public class entry1
    {
        public string id { get; set; }

        public string title { get; set; }

        public string description { get; set; }

        public string google_product_category { get; set; }

        public string product_type { get; set; }

        public string link { get; set; }

        public string image_link { get; set; }

        public string condition { get; set; }

        public string availability { get; set; }

        public string price { get; set; }

        public string brand { get; set; }

        public string gtin { get; set; }

        public string mpn { get; set; }

        public string shipping_weight { get; set; }



    }






    /// <summary>
    /// Logika interakcji dla klasy MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public class ConnexionExcel
        {
            public string _pathExcelFile;
            public ExcelQueryFactory _urlConnexion;
            public ConnexionExcel(string path)
            {
                this._pathExcelFile = path;
                this._urlConnexion = new ExcelQueryFactory(_pathExcelFile);
            }
            public string PathExcelFile
            {
                get
                {
                    return _pathExcelFile;
                }
            }
            public ExcelQueryFactory UrlConnexion
            {
                get
                {
                    return _urlConnexion;
                }
            }
        }
        private static Logger logger = LogManager.GetCurrentClassLogger();
        public MainWindow()
        {

          




                DateTime thisDay = DateTime.Now;
            var config = new NLog.Config.LoggingConfiguration();

            // Targets where to log to: File and Console
            var logfile = new NLog.Targets.FileTarget("logfile") { FileName = @"log\" + "log" + thisDay + ".txt" };

            var logconsole = new NLog.Targets.ConsoleTarget("logconsole");

            // Rules for mapping loggers to targets            
            config.AddRule(LogLevel.Info, LogLevel.Fatal, logconsole);
            config.AddRule(LogLevel.Debug, LogLevel.Fatal, logfile);
            NLog.LogManager.Configuration = config;
            InitializeComponent();
       
            //_stany = en.Stany_XML(2021, 2, 2).ToList();
           // sql ss = new sql();

            //var sss = ss.Opisy();
            //log oo = new log();

            if (Properties.Settings.Default.Autogeneracja_XML==1)
            {
                try
                {
                    MyXML();
                    CeneoXML();
                    FbXML();
                    IloscXml();
                    GoogleXml();
                    
                   
                }
                catch(Exception e)
                {
                    logger.Error("BLAD W AUTOMACIE");
                    logger.Error(e);
                }

            }
            if(Properties.Settings.Default.AUTOWYSYLANIE_FTP==1)
            {

                try
                {
                    UploadFtpFile(Properties.Settings.Default.FTP_FOLDER_UPLOAD, Properties.Settings.Default.SCIEZKA_XML_WLASNYXML);

                    UploadFtpFile(Properties.Settings.Default.FTP_FOLDER_UPLOAD, Properties.Settings.Default.SCIEZKA_XML_CENEO);

                    UploadFtpFile(Properties.Settings.Default.FTP_FOLDER_UPLOAD, Properties.Settings.Default.SCIEZKA_XML_FB);

                    UploadFtpFile(Properties.Settings.Default.FTP_FOLDER_UPLOAD, Properties.Settings.Default.SCIEZKA_XML_ILOSC);
                    UploadFtpFile(Properties.Settings.Default.FTP_FOLDER_UPLOAD, Properties.Settings.Default.SCIEZKA_XML_GOOGLE);
                }
                catch(Exception ee)
                {
                    logger.Error(ee);

                }


            }
            if(Properties.Settings.Default.AUTO_ZAMKNIECIE==1)
            {
                Close();
            }
         
            //   Window.Close();
      //      UploadFtpFile("xml", "C:\\Intel\\wlasnyxml.xml");

        

            //  foreach(Stany_XML_Result item in _stany)

            // {
            //   feed nn = new feed();
            // nn.title = item.Kod;

            //}
            //  _stanyPLUS = _stany.Where(e => e.Kod== "SEKATOR").ToList();
            //         string file = @"C:\Intel\test.xml";
            //       //        XElement xmlElements = new XElement("Branches", _stany.Select(i => new XElement("branch", new XAttribute("id", i))));
            //     var seria = new XmlSerializer(typeof(List<Stany_XML_Result>), new XmlRootAttribute("Produkty"));
            //   var writer = new FileStream(file, FileMode.Create);
            //  seria.Serialize(writer, _stany);
        }






        public void UploadFtpFile(string folderName, string fileName)
        {
            try
            {


                FtpWebRequest request;


                string absoluteFileName = System.IO.Path.GetFileName(fileName);

                request = WebRequest.Create(new Uri(string.Format(@"ftp://{0}/{1}/{2}", Properties.Settings.Default.FTP_ADRES, folderName, absoluteFileName))) as FtpWebRequest;
                request.Method = WebRequestMethods.Ftp.UploadFile;
                request.UseBinary = true;
                request.UsePassive = true;
                request.KeepAlive = true;
                request.Credentials = new NetworkCredential(Properties.Settings.Default.FTP_USER, Properties.Settings.Default.FTP_PASS);
                request.ConnectionGroupName = "group";

                using (FileStream fs = File.OpenRead(fileName))
                {
                    byte[] buffer = new byte[fs.Length];
                    fs.Read(buffer, 0, buffer.Length);
                    fs.Close();
                    Stream requestStream = request.GetRequestStream();
                    requestStream.Write(buffer, 0, buffer.Length);
                    requestStream.Flush();
                    requestStream.Close();
                }
            }
            catch (Exception e)
            {
                logger.Error("Błąd wysyłania FTP!");
                logger.Error(e);
            }

        }
        public void UploadFtpLog(string folderName, string fileName)
        {
            try
            {

       
            FtpWebRequest request;

            
            string absoluteFileName = System.IO.Path.GetFileName(fileName);

            request = WebRequest.Create(new Uri(string.Format(@"ftp://{0}/{1}/{2}", "ktd", folderName, absoluteFileName))) as FtpWebRequest;
            request.Method = WebRequestMethods.Ftp.UploadFile;
            request.UseBinary = true;
            request.UsePassive = true;
            request.KeepAlive = true;
            request.Credentials = new NetworkCredential(Properties.Settings.Default.FTP_USER, Properties.Settings.Default.FTP_PASS);
            request.ConnectionGroupName = "group";

            using (FileStream fs = File.OpenRead(fileName))
            {
                byte[] buffer = new byte[fs.Length];
                fs.Read(buffer, 0, buffer.Length);
                fs.Close();
                Stream requestStream = request.GetRequestStream();
                requestStream.Write(buffer, 0, buffer.Length);
                requestStream.Flush();
                requestStream.Close();
            }
            }
            catch(Exception e)
            {
                logger.Error("Błąd wysyłania FTP!");
                logger.Error(e);
            }

        }
        private void Button_Click(object sender, RoutedEventArgs e)
        {


            try { 
            
            string pathToExcelFile = @"";
            log oo = new log();
            bool login = oo.LogowanieAutomatyczne();
            bool todo = true;
            double ww;
            string foto = "";
            int  licznik = 0;
            int todolicznik = 0;
            int fails = 0;
            string foto1 = "";
            OpenFileDialog openFileDialog = new OpenFileDialog();
            if (openFileDialog.ShowDialog() == true)
                pathToExcelFile = openFileDialog.FileName;



            ConnexionExcel ConxObject = new ConnexionExcel(pathToExcelFile);
            //Query a worksheet with a header row  
            var query1 = from a in ConxObject.UrlConnexion.Worksheet<csv>()
                         select a;
            var qr = query1.ToList();
            qr = qr.Where(b => b.LP != null).ToList();


           // var qr = query1.Where(b => b.LP != null).ToList();
            try
            {


                foreach (var result in qr)


                {
                    
                     todolicznik = qr.Count;
                    string kod = result.Kod;
                    try
                    {

                        ww = double.Parse(result.waga);
                    }
                    catch (Exception a)
                    {
                        ww = 0;
                    }
                    //    string waga = result.waga;
                    try
                    {
                        foto = result.foto;
                        //		foto	"https://littledoctor.pl/webroot/delivery/images/STETO/LD_SteTime_fullsize.jpg https://littledoctor.pl/webroot/delivery/images/STETO/LD-SteTime__fullsize.jpg https://littledoctor.pl/webroot/delivery/images/ld_stetime_bag_270x270.jpg"	string
                        if (foto != null)
                        {
                            foto1 = foto.Replace(" ", ";");
                        }
                        else
                            foto1 = foto;
                    }
                    catch (Exception z)
                    {
                        foto1 = foto;
                    }

                   


                   
                  bool tocreate=  oo.Add(result.Kod, result.opis, result.STAN.ToString(), ww, foto1,result.Wymiary,result.MARKA);
                    if (tocreate == true)
                    {
                        licznik++;
                    }
                    else { fails++; }
               

                }

                MessageBox.Show("Zakończono!!\n Rekordów w pliku:" + todolicznik + "\nPrzetworzono poprawnie:" + licznik + "\nBłędów:" + fails);
                oo.Wylogowanie();
            }
            catch (Exception z)
            {

            }
            }
            catch(Exception ee)
            {
                logger.Error(ee);
            }

        }
      

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            int todolicznik = 0;
            int licznik = 0;
            int fails = 0;
            log oo = new log();
            bool login = oo.LogowanieAutomatyczne();
            bool todo = true;
            double ww;
            string foto = "";
            string foto1 = "";
            string pathToExcelFile = "";

            OpenFileDialog openFileDialog = new OpenFileDialog();
            if (openFileDialog.ShowDialog() == true)
                pathToExcelFile = openFileDialog.FileName;





            ConnexionExcel ConxObject = new ConnexionExcel(pathToExcelFile);
            //Query a worksheet with a header row  
            var query1 = from a in ConxObject.UrlConnexion.Worksheet<csv>()
                         select a;
            var qr = query1.ToList();
            qr = qr.Where(b => b.LP != null).ToList();
            todolicznik=qr.Count;
            foreach (var result in qr )

            {
                log oos = new log();
            bool ok=    oos.UpdateXMLQTY(result.Kod, result.STAN);
                if(ok==true)
                {
                    licznik++;
                }
                else
                { fails++; }
            }

            MessageBox.Show("Zakończono!!\n Rekordów w pliku:" + todolicznik + "\nPrzetworzono poprawnie:" + licznik + "\nBłędów:" + fails);
        }
        public void MyXML()
        {
            try
            {
                int ile = 0;
                Entities2 en = new Entities2();
            List<Prod> toxml = new List<Prod>();
            List<Stany_XML_Result> stany = en.Stany_XML(2021, 2, 2).ToList();
   
            foreach (Stany_XML_Result item in stany)
            {  

                
                Prod pd = new Prod();
                pd.Kod = item.Kod;

                pd.Nazwa = item.Nazwa;
                pd.Ean = item._EAN;
                pd.Grupa = item.Grupa;
                pd.Cena = item._CENA;
                    try
                    {
                        ile = item.link.Count(x => x == ';'); //w zmiennej k masz ilość wystąpień litery 'a' w stringu
                    }
                    catch(Exception eee)
                    {
                        logger.Error(eee);
                    }

                List<string> imgg = new List<string>();

                if (ile == 0)
                {
                    imgg.Add(item.link);

                }
             
                if (ile == 1)
                {

                    imgg.Add(item.link.Substring(0, item.link.IndexOf(";")));

                    imgg.Add(item.link.Substring(item.link.IndexOf(";") + 1, item.link.Length- item.link.IndexOf(";") - 1));


                }
                if (ile == 2)
                {
                    // img f = new img();
                    imgg.Add(item.link.Substring(0, item.link.IndexOf(";")));


                    int drugi = item.link.IndexOf(";");
                    int trzeci = item.link.LastIndexOf(";");
                    imgg.Add(item.link.Substring(drugi + 1, trzeci - drugi - 1));


                    //    ff.link = item.link.Substring(item.link.IndexOf(";"),item.link.LastIndexOf(";"));

                    imgg.Add(item.link.Substring(trzeci + 1, item.link.Length - trzeci - 1));
                    //imgg.Add(fff);

                }


                pd.imgs = imgg.ToArray();


                //pd.imgss = imgg.ToArray();






                // pd.link = item.link;
                pd.Stawka = item.Stawka;
                pd.wymiary = item.wymiary;
                pd.Opis = item.Opis.Replace("\n", "").Replace("\r", "");
                pd.waga = item.waga;
                pd.marka = item.marka;
                pd.Ilosc = item.xmlilosc;
                toxml.Add(pd);

            }


            string file = Properties.Settings.Default.SCIEZKA_XML_WLASNYXML;

            var seria = new XmlSerializer(typeof(List<Prod>), new XmlRootAttribute("Produkty"));
            var writer = new FileStream(file, FileMode.Create);
            seria.Serialize(writer, toxml);

            writer.Close();
            }
            catch (Exception e)
            {
                logger.Error(e);
            }

        }
        public static void uploadFolder(string source, string uploadpath)
        {
            WebRequest request = WebRequest.Create(uploadpath);
            request.Credentials = new NetworkCredential("ktad38904909_applogs", "@Azwsx123");
            string[] files = Directory.GetFiles(source, "*.*");
            string[] subFolders = Directory.GetDirectories(source);
            foreach (string file in files)
            {
                request = WebRequest.Create(file);
                request.Method = WebRequestMethods.Ftp.UploadFile;
            }
            foreach (string subFolder in subFolders)
            {
                request = WebRequest.Create(uploadpath + "/" + System.IO.Path.GetFileName(subFolder));
                request.Method = WebRequestMethods.Ftp.MakeDirectory; 
                request.Credentials = new NetworkCredential("ktad38904909_applogs", "@Azwsx123");
                uploadFolder(subFolder, uploadpath + "/" + System.IO.Path.GetFileName(subFolder));
            }
        }
        public void CeneoXML()
        {
            try
            {

            
            Entities2 en = new Entities2();
            List<offersO> toxml = new List<offersO>();

            List<Stany_XML_Result> stany = en.Stany_XML(2021, 2, 2).ToList();
            foreach (Stany_XML_Result item in stany)
            {
                offersO fd = new offersO();
                fd.id = Convert.ToUInt32(item.twrgid);
                fd.avail = Convert.ToInt32(item.xmlilosc);
            
                string waga = item.waga.Replace(".", ",");
                string linka = "";
                fd.name = item.Nazwa;
                fd.desc = item.Opis.Replace("\n", "").Replace("\r", "");
                fd.cat = item.Grupa;
                fd.price = Convert.ToDecimal(item._CENA);
                fd.url = item.link;
                fd.stock = Convert.ToUInt32(item.xmlilosc);
                fd.weightSpecified = true;
                fd.weight = Convert.ToDecimal(waga);
                fd.attrs = new offersOA[3];
                offersOA x = new offersOA() { name = "EAN", Value = item._EAN };
                offersOA xx = new offersOA() { name = "Kod_producenta", Value = "" };
                offersOA zz = new offersOA() { name = "Producent", Value = item.marka };
                fd.attrs[0] = x;
                fd.attrs[1] = xx;
                fd.attrs[2] = zz;
                try
                {
                    int ile = item.link.IndexOf(";");
                    linka = item.link.Substring(0, ile);
                }
                catch
                {
                    linka = item.link;
                }



                //fd.imgs = new offersOImgs();
                fd.imgs = new offersOImgs();
                offersOImgs aa = new offersOImgs();
                aa.main = new offersOImgsMain();
                offersOImgsMain wwww = new offersOImgsMain() { url = linka };
                aa.main = wwww;




                fd.imgs = aa;
                //fd.imgs.main.url = "aa";


                // fd.imgs = billAddress;

                // jak dodac imgs main ????
                //    < imgs >< main url = "https://ratujesz.pl/environment/cache/images/500_500_productGfx_822f63f4d2a62cf86a9e6d7cf739ffe1.jpg" /></ imgs >

                //  fd.imgs.main = new offersOImgsMain();


                toxml.Add(fd);
            }
            string file = Properties.Settings.Default.SCIEZKA_XML_CENEO;
            //    XElement xmlElements = new XElement("Branches", _stany.Select(i => new XElement("branch", new XAttribute("id", i))));
            var seria = new XmlSerializer(typeof(List<offersO>), new XmlRootAttribute("offers"));
            var settings = new XmlWriterSettings { OmitXmlDeclaration = true, Indent = false };
            XmlSerializerNamespaces ns = new XmlSerializerNamespaces();
            // ns.Add("");
            var writer = new FileStream(file, FileMode.Create);
            seria.Serialize(writer, toxml);
            writer.Close();
            }
            catch(Exception e)
            {
                logger.Error(e);
            }


        }
        public void FbXML()
        {
            string towar = "";
            try
            {

            
            Entities2 en = new Entities2();
                string file = Properties.Settings.Default.SCIEZKA_XML_FB;
            List<Group> toxml = new List<Group>();
            List<Employee> toxml1 = new List<Employee>();
            List<Stany_XML_Result> stany = en.Stany_XML(2021, 2, 2).ToList();
            int ids = 1;




            // Create the XmlSerializer.
            XmlSerializer s = new XmlSerializer(typeof(Group), new XmlRootAttribute("feed"));

            // To write the file, a TextWriter is required.
            TextWriter writer = new StreamWriter(file);

            /* Create an instance of the group to serialize, and set
               its properties. */
            Group group = new Group();
            group.Title = "LittleDoctor.pl";
            group.url = "wp.pl";
            LinkV2 l = new LinkV2();
            l.Url = "alternate";
            l.typ = "text/html";
            l.href = "ratujesz.pl";
            group.link = l;
            DateTime now = DateTime.Now;

            group.url = now.ToString();

            foreach (Stany_XML_Result item in stany)
            {
                    towar = item.Kod;
                Employee y = new Employee();
                Employee zz = new Employee();
                int ile = item.link.Count(x => x == ';'); 
                List<string> imgg = new List<string>();
                //   pd.link =new 
                if (ile == 0)
                {
                    y.image_link = item.link;

                }
                //https://littledoctor.pl/webroot/delivery/images/STETO/LD_SteTime_fullsize.jpg;https://littledoctor.pl/webroot/delivery/images/STETO/LD-SteTime__fullsize.jpg;https://littledoctor.pl/webroot/delivery/images/ld_stetime_bag_270x270.jpg
                if (ile == 1)
                {
                    string tab = item.link.Substring(item.link.IndexOf(";") + 1, item.link.Length- item.link.IndexOf(";") - 1);
                    y.additional_image_link = new string[1] { tab };
                    y.image_link = (item.link.Substring(0, item.link.IndexOf(";")));
                    y.additional_image_link[0] = item.link.Substring(item.link.IndexOf(";") + 1, item.link.Length - item.link.IndexOf(";") - 1);
                 //   imgg.Add  (item.link.Substring(item.link.IndexOf(";") + 1, item.link.Length - 1));


                }
                if (ile == 2)
                {
                    // img f = new img();
                    y.image_link = (item.link.Substring(0, item.link.IndexOf(";")));
                    

                    int drugi = item.link.IndexOf(";");
                    int trzeci = item.link.LastIndexOf(";");
                    string tab0 = item.link.Substring(drugi + 1, trzeci - drugi - 1);
                    string tab1 = item.link.Substring(trzeci + 1, item.link.Length - trzeci - 1);
                    y.additional_image_link = new string[2] { tab0, tab1 };
                   // y.additional_image_link = new string[2] {  };
                    //  y.additional_image_link[0] = item.link.Substring(drugi + 1, trzeci - drugi - 1);

                    //imgg.Add(item.link.Substring(drugi + 1, trzeci - drugi - 1));

                   // y.additional_image_link[1] =

                    //    ff.link = item.link.Substring(item.link.IndexOf(";"),item.link.LastIndexOf(";"));

                    // imgg.Add(item.link.Substring(trzeci + 1, item.link.Length - trzeci - 1));
                    //imgg.Add(fff);

                }
                if (ile == 3)
                {
                    // img f = new img();
                    y.image_link = (item.link.Substring(0, item.link.IndexOf(";")));


                    int drugi = item.link.IndexOf(";");
                    int trzeci = item.link.LastIndexOf(";");
                    string tab0 = item.link.Substring(drugi + 1, trzeci - drugi - 1);
                    string tab1 = item.link.Substring(trzeci + 1, item.link.Length - trzeci - 1);
                    y.additional_image_link = new string[2] { tab0, tab1 };
                    // y.additional_image_link = new string[2] {  };
                    //  y.additional_image_link[0] = item.link.Substring(drugi + 1, trzeci - drugi - 1);

                    //imgg.Add(item.link.Substring(drugi + 1, trzeci - drugi - 1));

                    // y.additional_image_link[1] =

                    //    ff.link = item.link.Substring(item.link.IndexOf(";"),item.link.LastIndexOf(";"));

                    // imgg.Add(item.link.Substring(trzeci + 1, item.link.Length - trzeci - 1));
                    //imgg.Add(fff);

                }
                y.availability = "in stock";
                y.inventory = item.xmlilosc;
             //   y.additional_image_link = imgg.ToArray();





                y.title = item.Nazwa;
                
              
                y.gtin = item._EAN;
                y.google_product_category = item.Grupa;
                y.price = item._CENA.ToString();
                y.description = item.Opis.Replace("\n", "").Replace("\r", "");
                y.shipping_weight = item.waga;
                y.id = item.twrgid.ToString();

                y.condition = "New";
                y.brand = item.marka;
                toxml1.Add(y);
              //  toxml1.Add(zz);

            }
            group.Employees = toxml1.ToArray();

            s.Serialize(writer, group);
            writer.Close();
            }
            catch(Exception e)
            {
                logger.Error(e);
                logger.Error(towar);
            }
        }

        public void IloscXml()
        {
            try { 
            Entities2 en = new Entities2();
            string file = Properties.Settings.Default.SCIEZKA_XML_ILOSC;
            List<Simple> toxml = new List<Simple>();
            List<Product> toxml1 = new List<Product>();
            List<Stany_XML_Result> stany = en.Stany_XML(2021, 2, 2).ToList();
            int ids = 1;
            // Create the XmlSerializer.
            XmlSerializer s = new XmlSerializer(typeof(Simple), new XmlRootAttribute("Products"));

            // To write the file, a TextWriter is required.
            TextWriter writer = new StreamWriter(file);
            Simple ss = new Simple();
            foreach (Stany_XML_Result item in stany)
            {
                Product p = new Product();
                p.gtin = item._EAN;
                p.price = item._CENA.ToString();
                p.Qty = Convert.ToInt32(item.xmlilosc);
                p.title = item.Nazwa;
                toxml1.Add(p);
            }
            ss.Products = toxml1.ToArray();
            s.Serialize(writer, ss);
            writer.Close();
            }
            catch(Exception e)
            { logger.Error(e); };
        }

        public void GoogleXml()
        {
            try
            { 
            Entities2 en = new Entities2();
            List<feed1> toxml = new List<feed1>();
            List<entry1> toxml1 = new List<entry1>();
            List<Stany_XML_Result> stany = en.Stany_XML(2021, 2, 2).ToList();
            feed1 ff = new feed1();
            ff.title = "test";
            foreach (Stany_XML_Result item in stany)
            {
                //  ff.entry = new entry1[1];

                entry1 xxx = new entry1() { brand = item.Nazwa, title = item.Nazwa, condition = "New", google_product_category = item.Grupa, gtin = item._EAN, description = item.Opis.Replace("\n", "").Replace("\r", ""), id = item.twrgid.ToString(),price=item._CENA.ToString(),shipping_weight=item.waga,availability="In stock" };
            

                //   ff.entry = new entry1[1];
                // entry1 xx = new entry1() { title = "aaa" };
                //ff.entry = xx;
                // ff.entry = new entry[2];
                // entry xx = new entry() { brand = item.marka };

                //ff.entry = xx;
                toxml1.Add(xxx);



                //   ff.


            }

            ff.entry = toxml1.ToArray();
            toxml.Add(ff);


                string file = Properties.Settings.Default.SCIEZKA_XML_GOOGLE;
            string[] test; ;
           
            //    XElement xmlElements = new XElement("Branches", _stany.Select(i => new XElement("branch", new XAttribute("id", i))));
            var seria = new XmlSerializer(typeof(List<entry1>), new XmlRootAttribute("feed"));

         //   XmlWriterSettings settings = new XmlWriterSettings() { OmitXmlDeclaration = true, Indent = true, Encoding = Encoding.UTF8 };
         //   StringBuilder output = new StringBuilder();
        //    XmlWriter writer = XmlWriter.Create(output, settings);
            XmlSerializerNamespaces xns = new XmlSerializerNamespaces();
            // xns.Add(string.Empty, "http://www.w3.org/2005/Atom");
            xns.Add("g", "http://base.google.com/ns/1.0");
           // writer = new XmlTextWriter(@"C:\intel\data.xml", null);
              var writer1 = new FileStream(file, FileMode.Create);
            seria.Serialize(writer1, toxml1, xns);
            writer1.Close();
                //    MessageBox.Show(output.ToString());
            }
            catch (Exception e)
            {
                logger.Error(e);
            }
        }

        private void Button_Click_2(object sender, RoutedEventArgs e)
        {

            MyXML
                ();
        }


       

        private void Button_Click_3(object sender, RoutedEventArgs e)
        {

            CeneoXML();
        }
    
        
        private void Button_Click_4(object sender, RoutedEventArgs e)
        {
            GoogleXml();
        }

        private void Button_Click_5(object sender, RoutedEventArgs e)
        {
            uint id = 1;
            Entities2 en = new Entities2();
          
               




                //  g.entryg = new entryg();


                //  

               







       
            
       
           
            //XmlRootAttribute ss = new XmlRootAttribute();
            //XmlAnyElementAttribute ss = new XmlAnyElementAttribute();

        }

        private void Button_Click_6(object sender, RoutedEventArgs e)
        {
            FbXML();
        }

        private void Button_Click_7(object sender, RoutedEventArgs e)
        {
            
        }

        private void Button_Click_8(object sender, RoutedEventArgs e)
        {
            IloscXml();
        }

        private void Window_Closed(object sender, EventArgs e)
        {
            try
            {
               // string path = @"/log";
            //    uploadFolder("ftp.ktad38904909.nazwa.pl", path);
                //UploadFtpLog("LITTLEDOCTORXMLOG",)
            }
            catch (Exception ee)
            {
                MessageBox.Show(ee.ToString()) ;
            }

        }

        private void Button_Click_9(object sender, RoutedEventArgs e)
        {
            int todolicznik = 0;
            int licznik = 0;
            int fails = 0;
            log oo = new log();
            bool login = oo.LogowanieAutomatyczne();
            bool todo = true;
            double ww;
            string foto = "";
            string foto1 = "";
            string pathToExcelFile = "";

            OpenFileDialog openFileDialog = new OpenFileDialog();
            if (openFileDialog.ShowDialog() == true)
                pathToExcelFile = openFileDialog.FileName;





            ConnexionExcel ConxObject = new ConnexionExcel(pathToExcelFile);
            //Query a worksheet with a header row  
            var query1 = from a in ConxObject.UrlConnexion.Worksheet<csv>()
                         select a;
            var qr = query1.ToList();
            qr = qr.Where(b => b.LP != null).ToList();
            todolicznik = qr.Count;
            foreach (var result in qr)

            {
                log oos = new log();
                bool ok = oos.UpdateXMLQTY(result.Kod, result.STAN);
                if (ok == true)
                {
                    licznik++;
                }
                else
                { fails++; }
            }

            MessageBox.Show("Zakończono!!\n Rekordów w pliku:" + todolicznik + "\nPrzetworzono poprawnie:" + licznik + "\nBłędów:" + fails);
        }

    }
    }
    

