﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace OXmlImporter
{
    [XmlType(TypeName = "Produkt")]
    public  class Prod
    {
        public string Kod { get; set; }
        public string Nazwa { get; set; }
        public string marka { get; set; }
        public string Grupa { get; set; }
        public string Ean { get; set; }
        [XmlArrayItem("img")]

        public string[] imgs { get; set; }
    
   

    public string waga { get; set; }
        public string Opis { get; set; }
        public string wymiary { get; set; }
        public Nullable<decimal> Cena { get; set; }
        public string Stawka { get; set; }
        public string Ilosc { get; set; }
        public string eanatry { get; set; }

       


  
    }
   
}
