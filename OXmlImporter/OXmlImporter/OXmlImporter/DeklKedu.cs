//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace OXmlImporter
{
    using System;
    using System.Collections.Generic;
    
    public partial class DeklKedu
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public DeklKedu()
        {
            this.DeklNag = new HashSet<DeklNag>();
        }
    
        public int DKK_DkkId { get; set; }
        public System.DateTime DKK_Data { get; set; }
        public string DKK_Nazwa { get; set; }
        public string DKK_Opis { get; set; }
        public byte DKK_Zablokowana { get; set; }
        public Nullable<int> DKK_OpeZalId { get; set; }
        public Nullable<int> DKK_StaZalId { get; set; }
        public System.DateTime DKK_TS_Zal { get; set; }
        public Nullable<int> DKK_OpeModId { get; set; }
        public Nullable<int> DKK_StaModId { get; set; }
        public System.DateTime DKK_TS_Mod { get; set; }
        public string DKK_OpeModKod { get; set; }
        public string DKK_OpeModNazwisko { get; set; }
        public string DKK_OpeZalKod { get; set; }
        public string DKK_OpeZalNazwisko { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<DeklNag> DeklNag { get; set; }
    }
}
