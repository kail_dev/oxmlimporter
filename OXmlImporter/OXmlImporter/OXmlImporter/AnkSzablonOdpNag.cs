//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace OXmlImporter
{
    using System;
    using System.Collections.Generic;
    
    public partial class AnkSzablonOdpNag
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public AnkSzablonOdpNag()
        {
            this.AnkSzablonOdpElem = new HashSet<AnkSzablonOdpElem>();
        }
    
        public int AnkSoN_AnkSonId { get; set; }
        public string AnkSoN_Nazwa { get; set; }
        public int AnkSoN_Auto { get; set; }
        public Nullable<int> AnkSoN_OpeZalID { get; set; }
        public Nullable<int> AnkSoN_StaZalId { get; set; }
        public System.DateTime AnkSoN_TS_Zal { get; set; }
        public Nullable<int> AnkSoN_OpeModID { get; set; }
        public Nullable<int> AnkSoN_StaModId { get; set; }
        public System.DateTime AnkSoN_TS_Mod { get; set; }
        public string AnkSoN_OpeModKod { get; set; }
        public string AnkSoN_OpeModNazwisko { get; set; }
        public string AnkSoN_OpeZalKod { get; set; }
        public string AnkSoN_OpeZalNazwisko { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<AnkSzablonOdpElem> AnkSzablonOdpElem { get; set; }
    }
}
