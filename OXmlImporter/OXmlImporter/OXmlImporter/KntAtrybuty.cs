//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace OXmlImporter
{
    using System;
    using System.Collections.Generic;
    
    public partial class KntAtrybuty
    {
        public int KnA_KnAId { get; set; }
        public int KnA_DeAId { get; set; }
        public Nullable<short> KnA_PodmiotTyp { get; set; }
        public Nullable<int> KnA_PodmiotId { get; set; }
        public string KnA_WartoscTxt { get; set; }
        public byte KnA_CzyKopiowac { get; set; }
        public byte KnA_CzyKod { get; set; }
        public byte KnA_CzyPrzenosic { get; set; }
        public byte KnA_CzyDrukowac { get; set; }
        public byte KnA_CzyKopiowacDoVAT { get; set; }
    
        public virtual DefAtrybuty DefAtrybuty { get; set; }
        public virtual PodmiotyView PodmiotyView { get; set; }
    }
}
